#include <stdio.h>
#include "balance.h"
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;
//ERROR, -1 
// 같은 코인 양팔에 올릴 때 
// 동전은 100개

//저울 1 a가크면 LARGE, 1; 
//저울 2 b가크면 SMALL, 0; 같으면 EQUAL, 2




int phase1(vector<int> &error_coin , vector<int> &normal_coin, vector<int> &same_coin, int &index);
int phase2(int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int &index);
int phase3(int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int &index);
void compare2(int ret, int a[2], int b[2],int clue, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin);
void compare4(int ret, int a[5], int b[5], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin);
void compare6(int ret, int a[4], int b[4], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin);
void compare12(int ret, int a[7], int b[7], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin);


int* at10() 
{
	vector<int> error_coin;
	vector<int> normal_coin;
	//100,2짜리 선언
	vector<int> same_coin;
	int index = 0;
	int wwf[4] = { -1,-1,-1,-1 };

	while (error_coin.size() < 1) {

		if (phase1(error_coin, normal_coin, same_coin, index) == 2) {
			if (phase2(1, error_coin, normal_coin, same_coin, index) == 2) {
				phase3(1, error_coin, normal_coin, same_coin, index);
			}
		}

		//만약 불량동전이 0번이나 1번이라 1,1개씩 밖에 못만들면
		while (normal_coin.size() < 2) {
			int a[2];
			int b[2];
			a[0] = error_coin[0]; a[1] = -1;
			b[0] = index++; b[1] = -1;
			int ret = balance(a, b);
			compare2(ret, a, b, 3, error_coin, normal_coin, same_coin);
		}
	}

	wwf[0] = normal_coin[0];
	wwf[1] = normal_coin[1];
	wwf[2] = error_coin[0];

	//wwf로 비교 -> w 5개 모을때까지만.
	if (normal_coin.size() < 5) {
		for (; index < 98; index = index + 3) {
			int a4[4]; a4[0] = index; a4[1] = index + 1; a4[2] = index + 2; a4[3] = -1;
			int ret = balance(wwf, a4);
			compare6(ret, wwf, a4, 1, error_coin, normal_coin, same_coin);
			if (normal_coin.size() > 5) {
				index = index + 3;
				break;
			}
		}
	}

	//wwwwwf로 비교
	for (; index < 95; index = index + 6) {
		//cout << index << " " << index + 5 << "\n";
		int a7[7]; a7[0] = index; a7[1] = index + 1; a7[2] = index + 2; a7[3] = index + 3; a7[4] = index + 4; a7[5] = index + 5; a7[6] = -1;
		int wwwwwf[7]; wwwwwf[0] = normal_coin[0]; wwwwwf[1] = normal_coin[1]; wwwwwf[2] = normal_coin[2]; wwwwwf[3] = normal_coin[3]; wwwwwf[4] = normal_coin[4]; 
		wwwwwf[5] = error_coin[0]; wwwwwf[6] = -1;
		int ret = balance(wwwwwf, a7);
		compare12(ret, wwwwwf, a7, 1, error_coin, normal_coin, same_coin);
	}

	//마무리작업
	for (; index < 98; index = index + 3) {
		int a4[4]; a4[0] = index; a4[1] = index + 1; a4[2] = index + 2; a4[3] = -1;
		int ret = balance(wwf, a4);
		compare6(ret, wwf, a4, 1, error_coin, normal_coin, same_coin);
	}
	if (index == 98) {
		int a[3]; a[0] = normal_coin[0]; a[1] = error_coin[1]; a[2] = -1;
		int b[3]; b[0] = index; b[1] = index + 1; b[2] = -1;
		int ret = balance(a, b);

		switch (ret)
		{
			//뒤에가 무거움 = ww
		case 0:
			normal_coin.push_back(b[0]);
			normal_coin.push_back(b[1]);
			break;

			//뒤에가 가벼움 = ff
		case 1:
			error_coin.push_back(b[0]);
			error_coin.push_back(b[1]);
			break;

			//둘다 wf
		case 2: {
			int a1[2];
			int b1[2];
			a1[0] = b[0]; a1[1] = -1;
			b1[0] = b[1]; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 2, error_coin, normal_coin, same_coin);
			break;
		}
		default:
			"compare2 errors";
			break;
		}
	}
	else if (index == 99) {
		int a[2];
		int b[2];
		a[0] = error_coin[0]; a[1] = -1;
		b[0] = index++; b[1] = -1;
		int ret = balance(a, b);
		compare2(ret, a, b, 3, error_coin, normal_coin, same_coin);
	}

	int size = error_coin.size();
	int* ans = (int*)malloc(sizeof(int) * (size + 1));;
	for (int i = 0; i < size; i++) {
		ans[i] = error_coin[i];
	}
	ans[size] = -1;

	/*for (int i = 0; i < error_coin.size(); i++) {
		cout << error_coin[i] << " ";
	}
	cout << "\n ";
	for (int i = 0; i < normal_coin.size(); i++) {
		cout << normal_coin[i] << " ";
	}*/
	return ans;
}
int* at90()
{
	vector<int> error_coin;
	vector<int> normal_coin;
	//100,2짜리 선언
	vector<int> same_coin;
	int index = 0;
	int wff[4] = { -1,-1,-1,-1 };

	while (normal_coin.size() < 1) {

		if (phase1(error_coin, normal_coin, same_coin, index) == 2) {
			if (phase2(2, error_coin, normal_coin, same_coin, index) == 2) {
				phase3(2, error_coin, normal_coin, same_coin, index);
			}
		}

		//만약 정상동전이 0번이나 1번이라 1,1개씩 밖에 못만들면
		while (error_coin.size() < 2) {
			int a[2];
			int b[2];
			a[0] = error_coin[0]; a[1] = -1;
			b[0] = index++; b[1] = -1;
			int ret = balance(a, b);
			compare2(ret, a, b, 3, error_coin, normal_coin, same_coin);
		}
	}

	wff[0] = normal_coin[0];
	wff[1] = error_coin[1];
	wff[2] = error_coin[0];

	//wff로 비교 -> f 5개 모을때까지만.
	if (error_coin.size() < 5) {
		for (; index < 98; index = index + 3) {
			int a4[4]; a4[0] = index; a4[1] = index + 1; a4[2] = index + 2; a4[3] = -1;
			int ret = balance(wff, a4);
			compare6(ret, wff, a4, 2, error_coin, normal_coin, same_coin);
			if (error_coin.size() > 5) {
				index = index + 3;
				break;
			}
		}
	}

	//wfffff로 비교
	for (; index < 95; index = index + 6) {
		//cout << index << " " << index + 5 << "\n";
		int a7[7]; a7[0] = index; a7[1] = index + 1; a7[2] = index + 2; a7[3] = index + 3; a7[4] = index + 4; a7[5] = index + 5; a7[6] = -1;
		int wfffff[7]; wfffff[0] = error_coin[0]; wfffff[1] = error_coin[1]; wfffff[2] = error_coin[2]; wfffff[3] = error_coin[3]; wfffff[4] = error_coin[4];
		wfffff[5] = normal_coin[0]; wfffff[6] = -1;
		int ret = balance(wfffff, a7);
		compare12(ret, wfffff, a7, 2, error_coin, normal_coin, same_coin);
	}

	wff[0] = normal_coin[0];
	wff[1] = error_coin[1];
	wff[2] = error_coin[0];

	//마무리작업
	for (; index < 98; index = index + 3) {
		int a4[4]; a4[0] = index; a4[1] = index + 1; a4[2] = index + 2; a4[3] = -1;
		int ret = balance(wff, a4);
		compare6(ret, wff, a4, 2, error_coin, normal_coin, same_coin);
	}
	if (index == 98) {
		int a[3]; a[0] = normal_coin[0]; a[1] = error_coin[1]; a[2] = -1;
		int b[3]; b[0] = index; b[1] = index + 1; b[2] = -1;
		int ret = balance(a, b);

		switch (ret)
		{
			//뒤에가 무거움 = ww
		case 0:
			normal_coin.push_back(b[0]);
			normal_coin.push_back(b[1]);
			break;

			//뒤에가 가벼움 = ff
		case 1:
			error_coin.push_back(b[0]);
			error_coin.push_back(b[1]);
			break;

			//둘다 wf
		case 2: {
			int a1[2];
			int b1[2];
			a1[0] = b[0]; a1[1] = -1;
			b1[0] = b[1]; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 2, error_coin, normal_coin, same_coin);
			break;
		}
		default:
			"compare2 errors";
			break;
		}
	}
	else if (index == 99) {
		int a[2];
		int b[2];
		a[0] = error_coin[0]; a[1] = -1;
		b[0] = index++; b[1] = -1;
		int ret = balance(a, b);
		compare2(ret, a, b, 3, error_coin, normal_coin, same_coin);
	}

	int size = error_coin.size();
	int* ans = (int*)malloc(sizeof(int) * (size + 1));;
	for (int i = 0; i < size; i++) {
		ans[i] = error_coin[i];
	}
	ans[size] = -1;


	sort(ans, ans + size);
	/*for (int i = 0; i < size; i++) {
		cout << ans[i] << " ";
	}
	cout << "\n ";
	for (int i = 0; i < normal_coin.size(); i++) {
		cout << normal_coin[i] << " ";
	}*/
	return ans;
}


int phase1(vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int &index) {

	int a[2];
	int b[2];
	a[0] = index++; a[1] = -1;
	b[0] = index++; b[1] = -1;
	int ret = balance(a, b);
	compare2(ret, a, b, 2, error_coin, normal_coin, same_coin);
	return ret;
}
int phase2(int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int &index) {
	int a[3];
	int b[3];
	int ret = 0;
	if (ver == 1) {
		for (int i = 0; i < 2; i++) {
			a[i] = same_coin[i];
		}
		for (int i = 0; i < 2; i++) {
			b[i] = index++;
		}
		a[2] = -1; b[2] = -1;
		same_coin.clear();
		ret = balance(a, b);

		switch (ret)
		{
		case 0: {//a에 불량동전이었던거임 -> b는 ww or wf
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 1, error_coin, normal_coin, same_coin);

			for (int i = 0; i < 2; i++) {
				error_coin.push_back(a[i]);
			}
		} 
			break;
		case 1: { //b에 불량동전  wf or ff
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 0, error_coin, normal_coin, same_coin);

			for (int i = 0; i < 2; i++) {
				normal_coin.push_back(a[i]);
			}
		}
			  break;
		case 2: {
			for (int i = 0; i < 2; i++) {
				same_coin.push_back(a[i]);
				same_coin.push_back(b[i]);
			}
		}
			  break;
		default:
			printf("error occurs in p2");
			break;
		}
	}
	else if (ver == 2) {
		for (int i = 0; i < 2; i++) {
			a[i] = same_coin[i];
		}
		for (int i = 0; i < 2; i++) {
			b[i] = index++;
		}
		a[2] = -1; b[2] = -1;
		same_coin.clear();
		ret = balance(a, b);

		switch (ret)
		{
		case 0: //b에 정상동전 존재

		{ //b에 정상동전  wf or ww
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 1, error_coin, normal_coin, same_coin);

			for (int i = 0; i < 2; i++) {
				error_coin.push_back(a[i]);
			}
		}

		break;
		//a가 모두 정상동전이었던거임. -> b는 ffor wf
		case 1:
		{
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 0, error_coin, normal_coin, same_coin);
			for (int i = 0; i < 2; i++) {
				normal_coin.push_back(a[i]);
			}
		}

		break;
		case 2: { //진짜 극악의 확률로 a = b = wwww
			for (int i = 0; i < 2; i++) {
				same_coin.push_back(a[i]);
				same_coin.push_back(b[i]);
			}
		}
			  break;
		default:
			printf("error occurs in p2");
			break;
		}
	}
	return ret;
}
int phase3(int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int &index) {
	int a[5];
	int b[5];
	int ret = 0;
	if (ver == 1) {
		for (int i = 0; i < 4; i++) {
			a[i] = same_coin[i];
		}
		for (int i = 0; i < 4; i++) {
			b[i] = index++;
		}
		a[4] = -1; b[4] = -1;
		ret = balance(a, b);

		switch (ret)
		{
			//a가 모두 불량동전이었던거임.
		case 0: {
			for (int i = 0; i < 4; i++) {
				error_coin.push_back(a[i]);
			}
			//b에 정상동전 (b4개 비교) -> ffff 1234 -> ff와 12 먼저 비교
			int a1[3]; a1[0] = a[0]; a1[1] = a[1]; a1[2] = -1;
			int b1[3]; b1[0] = b[0]; b1[1] = b[1]; b1[2] = -1;
			int ret2 = balance(a1, b1);
			compare4(ret2, a, b, 2,error_coin, normal_coin, same_coin);
			break;
		}
			

		case 1: {//b에 불량동전 (b4개 비교) -> wwww 1234 -> ww와 12 먼저 비교
			int a1[3]; a1[0] = a[0]; a1[1] = a[1]; a1[2] = -1;
			int b1[3]; b1[0] = b[0]; b1[1] = b[1]; b1[2] = -1;
			int ret2 = balance(a1, b1);
			compare4(ret2, a, b, 1,error_coin, normal_coin, same_coin);
			break;
		}

		case 2: {
			for (int i = 0; i < 4; i++) {
				same_coin.push_back(b[i]);
			}
		}
			  break;
		default:
			printf("error occurs in p3");
			break;
		}
		//8개가 다 F일 확률은 극히 낮음.
		for (int i = 0; i < same_coin.size(); i++) {
			normal_coin.push_back(same_coin[i]);
		}
		same_coin.clear();
	}
	else if (ver == 2) {
		for (int i = 0; i < 4; i++) {
			a[i] = same_coin[i];
		}
		for (int i = 0; i < 4; i++) {
			b[i] = index++;
		}
		a[4] = -1; b[4] = -1;
		ret = balance(a, b);

		switch (ret)
		{
		case 0: { //b에 정상동전 (b4개 비교) -> ffff 1234 -> ff와 12 먼저 비교
			int a1[3]; a1[0] = a[0]; a1[1] = a[1]; a1[2] = -1;
			int b1[3]; b1[0] = b[0]; b1[1] = b[1]; b1[2] = -1;
			int ret2 = balance(a1, b1);
			compare4(ret2, a, b, 2,error_coin, normal_coin, same_coin);
		}
			  break;

		case 1: {//a가 모두 정상동전이었음.
			for (int i = 0; i < 4; i++) {
				error_coin.push_back(a[i]);
			} //b에 불량동전 wwww 1234 ww와 12먼저 비교
			int a1[3]; a1[0] = a[0]; a1[1] = a[1]; a1[2] = -1;
			int b1[3]; b1[0] = b[0]; b1[1] = b[1]; b1[2] = -1;
			int ret2 = balance(a1, b1);
			compare4(ret2, a, b, 1,error_coin, normal_coin, same_coin);
		}
			  break;
		case 2: {
			for (int i = 0; i < 4; i++) {
				same_coin.push_back(b[i]);
			}
		}
			  break;
		default:
			printf("error occurs in p3");
			break;
		}
		//8개가 다 W일 확률은 극히 낮음.
		for (int i = 0; i < same_coin.size(); i++) {
			error_coin.push_back(same_coin[i]);
		}
		same_coin.clear();
	}
	return ret;
}
void compare2(int ret, int a[2], int b[2], int clue, vector<int>& error_coin, vector<int>& normal_coin , vector<int>& same_coin) {

	//ff or fw이다.
	if (clue == 0) {
		switch (ret)
		{
			//앞에가 가벼움
		case 0:
			error_coin.push_back(a[0]);
			normal_coin.push_back(b[0]);
			break;

		case 1:
			error_coin.push_back(b[0]);
			normal_coin.push_back(a[0]);
			break;
		case 2:
			error_coin.push_back(a[0]);
			error_coin.push_back(b[0]);
			break;
		default:
			printf("error occurs");
			break;
		}
	}

	//ww or fw이다.
	else if (clue == 1) {

		switch (ret)
		{
			//앞에가 가벼움
		case 0:
			error_coin.push_back(a[0]);
			normal_coin.push_back(b[0]);
			break;

			//뒤에가 가벼움
		case 1:
			error_coin.push_back(b[0]);
			normal_coin.push_back(a[0]);
			break;

		case 2:
			normal_coin.push_back(a[0]);
			normal_coin.push_back(b[0]);
			break;
		default:
			printf("error occurs");
			break;
		}
	}
	//a는 error인 기준이니깐 넣지마라
	else if (clue == 3) {
		switch (ret)
		{
			//앞에가 가벼움
		case 0:
			normal_coin.push_back(b[0]);
			break;

			//뒤에가 가벼움
		case 1:
			error_coin.push_back(b[0]);
			break;

		case 2:
			error_coin.push_back(b[0]);
			break;
		default:
			printf("error occurs");
			break;
		}
	}
	else {
		switch (ret)
		{
			//앞에가 가벼움
		case 0:
			error_coin.push_back(a[0]);
			normal_coin.push_back(b[0]);
			break;
		case 1:
			error_coin.push_back(b[0]);
			normal_coin.push_back(a[0]);
			break;
		case 2:
			same_coin.push_back(a[0]);
			same_coin.push_back(b[0]);
			break;
		default:
			printf("error occurs");
			break;
		}
	}
}
void compare4(int ret, int a[5], int b[5], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin) {
	//w가 많을때
	if (ver == 1) {
		switch (ret)
		{
			//앞에가 가벼움
		case 0:
			printf("error occurs in c4_0");
			break;

			//앞에가 무거움
		case 1: {//그럼 0,1은 wf or ff
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 0, error_coin, normal_coin, same_coin);

			//뒤에 2,3은 ww or wf or ff
			int a2[3]; a2[0] = error_coin[0]; a2[1] = normal_coin[0]; a2[2] = -1;
			int b2[3]; b2[0] = b[2]; b2[1] = b[3]; b2[2] = -1;
			ret2 = balance(a2, b2);

			switch (ret2)
			{
				//뒤에가 무거움 = ww
			case 0:
				normal_coin.push_back(b2[0]);
				normal_coin.push_back(b2[1]);
				break;

				//앞에가 가벼움 = ff
			case 1:
				error_coin.push_back(b2[0]);
				error_coin.push_back(b2[1]);
				break;

				//둘다 wf
			case 2: {
				a1[0] = b2[0]; b1[0] = b2[1];
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 0, error_coin, normal_coin, same_coin);
				break;
			}
			default:
				"compare2 errors";
				break;
			}
		}
			  break;
			  //얘네 둘도 정상
		case 2: {
			normal_coin.push_back(b[0]);
			normal_coin.push_back(b[1]);

			//그럼 뒤에 애는 wf 아니면 ff
			int a1[2]; int b1[2];
			a1[0] = b[2]; b1[0] = b[3]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 0, error_coin, normal_coin, same_coin);
		}

			  break;
		default:
			"compare2 errors";
			break;
		}
	}
	//f가 많을때
	else if (ver == 2) {
		switch (ret)
		{
			//앞에가 가벼움
		case 0: { //그럼 0,1은 wf or ww
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 1, error_coin, normal_coin, same_coin);

			//뒤에 2,3은 ww or wf or ff
			int a2[3]; a2[0] = error_coin[0]; a2[1] = normal_coin[0]; a2[2] = -1;
			int b2[3]; b2[0] = b[2]; b2[1] = b[3]; b2[2] = -1;
			ret2 = balance(a2, b2);

			switch (ret2)
			{
				//뒤에가 무거움 = ww
			case 0:
				normal_coin.push_back(b2[0]);
				normal_coin.push_back(b2[1]);
				break;

				//앞에가 가벼움 = ff
			case 1:
				error_coin.push_back(b2[0]);
				error_coin.push_back(b2[1]);
				break;

				//둘다 wf
			case 2: {
				a1[0] = b2[0]; b1[0] = b2[1];
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 0, error_coin, normal_coin, same_coin);
				break;
			}
			default:
				"compare2 errors";
				break;
			}

		}
			  break;
			  //앞에가 무거움
		case 1: {
			printf("error occurs in c4_1");
			break;
		}
			  break;
			  //얘네 둘도 불량
		case 2: {
			error_coin.push_back(b[0]);
			error_coin.push_back(b[1]);

			//그럼 뒤에 애는 wf 아니면 ww
			int a1[2]; int b1[2];
			a1[0] = b[2]; b1[0] = b[3]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 1, error_coin, normal_coin, same_coin);
		}
			  break;
		default:
			"compare2 errors";
			break;
		}
	}


}
void compare6(int ret, int a[4], int b[4], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin) {
	int ret2 = 0;
	int a2[2] = { 0,-1 };
	int b2[2] = { 0,-1 };
	a2[0] = b[0];
	b2[0] = b[1];

	//ver 1 = wwf이다.
	if (ver == 1) {
		switch (ret)
		{
			//뒤에가 무거움 = www
		case 0: {
			normal_coin.push_back(b[0]);
			normal_coin.push_back(b[1]);
			normal_coin.push_back(b[2]);
		}
			break;
		
			  //뒤에가 가벼운 = wff / fff
		case 1:
			ret2 = balance(a2, b2);

			//둘이 같음 = FF -> a[2] = w or f
			if (ret2 == 2) {
				error_coin.push_back(b[0]);
				a2[0] = b[2];
				int ret3 = balance(a2, b2);
				compare2(ret3, a2, b2, 0, error_coin, normal_coin, same_coin);
			}
			//a[0] = w, a[1] = f, a[2] = f
			else if (ret2 == 1) {
				error_coin.push_back(b[2]);
				normal_coin.push_back(b[0]);
				error_coin.push_back(b[1]);
			}
			//b[0] = f, b[1] = w, b[2] = f
			else if (ret2 == 0) {
				error_coin.push_back(b[2]);
				normal_coin.push_back(b[1]);
				error_coin.push_back(b[0]);
			}
			break;

			//얘도 wwf
		case 2:
			ret2 = balance(a2, b2);
			//b[0] = b[1] =w b[2] = f
			if (ret2 == 2) {
				normal_coin.push_back(b[0]);
				normal_coin.push_back(b[1]);
				error_coin.push_back(b[2]);
			}
			else if (ret2 == 1) {
				error_coin.push_back(b[1]);
				normal_coin.push_back(b[0]);
				normal_coin.push_back(b[2]);
			}
			else if (ret2 == 0) {
				error_coin.push_back(b[0]);
				normal_coin.push_back(b[1]);
				normal_coin.push_back(b[2]);
			}
			else {
				cout << "errrrorr in compare6-2";
			}
			break;
		default:
			cout << "errrrorr in compare6-1";
			break;
		}
	}
	//wff이다
	else if (ver == 2) {
		switch (ret)
		{
			//뒤에가 무거움 = wwf, www
		case 0: {
			ret2 = balance(a2, b2);

			//둘이 같음 = ww -> a[2] = w or f
			if (ret2 == 2) {
				normal_coin.push_back(b[0]);
				a2[0] = b[2];
				int ret3 = balance(a2, b2);
				compare2(ret3, a2, b2, 1, error_coin, normal_coin, same_coin);
			}
			//b[0] = w, b[1] = f, b[2] = w
			else if (ret2 == 1) {
				normal_coin.push_back(b[2]);
				normal_coin.push_back(b[0]);
				error_coin.push_back(b[1]);
			}
			//b[0] = f, b[1] = w, b[2] = w
			else if (ret2 == 0) {
				normal_coin.push_back(b[2]);
				normal_coin.push_back(b[1]);
				error_coin.push_back(b[0]);
			}

			break;
		}
			  //뒤에가 가벼운 = fff
		case 1:
			error_coin.push_back(b[0]);
			error_coin.push_back(b[1]);
			error_coin.push_back(b[2]);
			
			break;

			//얘도 wff
		case 2:
			ret2 = balance(a2, b2);
			//b[0] = b[1] =f b[2] = w
			if (ret2 == 2) {
				error_coin.push_back(b[0]);
				error_coin.push_back(b[1]);
				normal_coin.push_back(b[2]);
			}
			else if (ret2 == 1) {
				error_coin.push_back(b[1]);
				normal_coin.push_back(b[0]);
				error_coin.push_back(b[2]);
			}
			else if (ret2 == 0) {
				error_coin.push_back(b[0]);
				normal_coin.push_back(b[1]);
				error_coin.push_back(b[2]);
			}
			else {
				cout << "errrrorr in compare6-2";
			}
			break;
		default:
			cout << "errrrorr in compare6-1";
			break;
		}
	}

}
void compare12(int ret, int a[7], int b[7], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin) 
{
	//wwwwwf
	if (ver == 1) {
		switch (ret)
		{
			//뒤가 무거움-> wwwwww
		case 0:
			for (int i = 0; i < 6; i++) {
				normal_coin.push_back(b[i]);
			}
			break;
			//앞에가 무거움 -> f가 하나 이상 있다는 것만 알음
		case 1: {
			int wwf[4] = { -1,-1,-1,-1 };
			wwf[0] = normal_coin[0];
			wwf[1] = normal_coin[1];
			wwf[2] = error_coin[0];

			int a3[4]; int b3[4];
			a3[0] = b[0]; a3[1] = b[1]; a3[2] = b[2]; a3[3] = -1;
			b3[0] = b[3]; b3[1] = b[4]; b3[2] = b[5]; b3[3] = -1;

			int ret2 = balance(wwf, a3);
			compare6(ret2, wwf, a3, 1, error_coin, normal_coin, same_coin);

			//a3 = www , b3은 wff or fff
			if (ret2 == 0) {
				int a2[2] = { 0,-1 };
				int b2[2] = { 0,-1 };
				a2[0] = b3[0];
				b2[0] = b3[1];
				int ret3 = balance(a2, b2);
				//둘이 같음 = FF -> a[2] = w or f
				if (ret3 == 2) {
					error_coin.push_back(b3[0]);
					a2[0] = b3[2];
					int ret4 = balance(a2, b2);
					compare2(ret4, a2, b2, 0, error_coin, normal_coin, same_coin);
				}
				//b3[0] = w, b3[1] = f, b3[2] = f
				else if (ret3 == 1) {
					error_coin.push_back(b3[2]);
					normal_coin.push_back(b3[0]);
					error_coin.push_back(b3[1]);
				}
				//b3[0] = f, b3[1] = w, b3[2] = f
				else if (ret3 == 0) {
					error_coin.push_back(b3[2]);
					normal_coin.push_back(b3[1]);
					error_coin.push_back(b3[0]);
				}
			}

			//a3 = wff or fff -> b3은 아무거나
			else if (ret2 == 1) {
				int ret3 = balance(wwf, b3);
				compare6(ret3, wwf, b3, 1, error_coin, normal_coin, same_coin);
			}

			//a3 = wwf -> b3은 wwf, wff, fff
			else if (ret2 == 2) {
				wwf[1] = error_coin[1]; //wwf -> wff로 바꿈
				int ret3 = balance(wwf, b3);

				if (ret3 == 2) {	//wff
					int a2[2] = { 0,-1 };
					int b2[2] = { 0,-1 };
					a2[0] = b3[0];
					b2[0] = b3[1];
					int ret4 = balance(a2, b2);
					//b3[0] = b3[1] =f b3[2] = w
					if (ret4 == 2) {
						error_coin.push_back(b3[0]);
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[2]);
					}
					else if (ret4 == 1) {
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[0]);
						error_coin.push_back(b3[2]);
					}
					else if (ret4 == 0) {
						error_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						error_coin.push_back(b3[2]);
					}
					else {
						cout << "errrrorr in compare12-2-2";
					}
				}
				else if (ret3 == 1) {	//fff
					error_coin.push_back(b3[0]);
					error_coin.push_back(b3[1]);
					error_coin.push_back(b3[2]);
				}
				else if (ret3 == 0) { //wwf
					int a2[2] = { 0,-1 };
					int b2[2] = { 0,-1 };
					a2[0] = b3[0];
					b2[0] = b3[1];
					int ret4 = balance(a2, b2);
					//b3[0] = b3[1] =w b3[2] = f
					if (ret4 == 2) {
						normal_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						error_coin.push_back(b3[2]);
					}
					else if (ret4 == 1) {
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[0]);
						normal_coin.push_back(b3[2]);
					}
					else if (ret4 == 0) {
						error_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						normal_coin.push_back(b3[2]);
					}
					else {
						cout << "errrrorr in compare12-2-0";
					}
				}
			}
		}
			break;
			//f 1개 껴있음-> a,b,c로 나누어서
		case 2: {
			int a2[3]; int b2[3]; int c2[3];

			a2[0] = b[0]; a2[1] = b[1]; a2[2] = -1;
			b2[0] = b[2]; b2[1] = b[3]; b2[2] = -1;
			c2[0] = b[4]; c2[1] = b[5]; c2[2] = -1;

			int ret2 = balance(a2, b2);
			//둘다 ww ww였던것
			if (ret2 == 2) {

				for (int i = 0; i < 2; i++) {
					normal_coin.push_back(a2[i]);
					normal_coin.push_back(b2[i]);
				}	//c비교
				int a1[2]; int b1[2];
				a1[0] = c2[0]; b1[0] = c2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 1, error_coin, normal_coin, same_coin);
			}
			else if (ret2 == 1) {
				for (int i = 0; i < 2; i++) {
					normal_coin.push_back(a2[i]);
					normal_coin.push_back(c2[i]);
				}
				//b비교
				int a1[2]; int b1[2];
				a1[0] = b2[0]; b1[0] = b2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 1, error_coin, normal_coin, same_coin);
			}
			else if (ret2 == 0) {
				for (int i = 0; i < 2; i++) {
					normal_coin.push_back(b2[i]);
					normal_coin.push_back(c2[i]);
				}
				//a비교
				int a1[2]; int b1[2];
				a1[0] = a2[0]; b1[0] = a2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 1, error_coin, normal_coin, same_coin);
			}
		}
			  break;
		default:
			printf("error occurs");
			break;
		
		}
	}
	else if (ver == 2) {
		switch (ret)
		{
			//뒤가 무거움 -> w가 하나 이상 있다는 것만 알음.
		case 0: {
			int wff[4] = { -1,-1,-1,-1 };
			wff[0] = normal_coin[0];
			wff[1] = error_coin[1];
			wff[2] = error_coin[0];

			int a3[4]; int b3[4];
			a3[0] = b[0]; a3[1] = b[1]; a3[2] = b[2]; a3[3] = -1;
			b3[0] = b[3]; b3[1] = b[4]; b3[2] = b[5]; b3[3] = -1;
			int ret2 = balance(wff, a3);
			compare6(ret2, wff, a3, 2, error_coin, normal_coin, same_coin);

			//a3 = wwf, www , b3은 아무거나
			if (ret2 == 0) {
				int ret3 = balance(wff, b3);
				compare6(ret3, wff, b3, 2, error_coin, normal_coin, same_coin);
			}
			//a3 = fff, b3 -> wwf, www
			else if (ret2 == 1) {
				int a2[2] = { 0,-1 };
				int b2[2] = { 0,-1 };
				a2[0] = b3[0];
				b2[0] = b3[1];
				int ret3 = balance(a2, b2);
				//둘이 같음 = ww -> a[2] = w or f
				if (ret3 == 2) {
					normal_coin.push_back(b3[0]);
					a2[0] = b3[2];
					int ret4 = balance(a2, b2);
					compare2(ret4, a2, b2, 1, error_coin, normal_coin, same_coin);
				}
				//b3[0] = w, b3[1] = f, b3[2] = w
				else if (ret3 == 1) {
					error_coin.push_back(b3[1]);
					normal_coin.push_back(b3[0]);
					normal_coin.push_back(b3[2]);
				}
				//b3[0] = f, b3[1] = w, b3[2] = w
				else if (ret3 == 0) {
					error_coin.push_back(b3[0]);
					normal_coin.push_back(b3[1]);
					normal_coin.push_back(b3[2]);
				}
			}
			//a3 = wff,-> b3 www wwf wff
			else if (ret2 == 2) {
				wff[1] = normal_coin[1]; //wff wwf로 바꿈
				int ret3 = balance(wff, b3);

				if (ret3 == 2) {	//wwf
					int a2[2] = { 0,-1 };
					int b2[2] = { 0,-1 };
					a2[0] = b3[0];
					b2[0] = b3[1];
					int ret4 = balance(a2, b2);
					//b3[0] = b3[1] =w b3[2] = f
					if (ret4 == 2) {
						normal_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						error_coin.push_back(b3[2]);
					}
					else if (ret4 == 1) {
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[0]);
						normal_coin.push_back(b3[2]);
					}
					else if (ret4 == 0) {
						error_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						normal_coin.push_back(b3[2]);
					}
					else {
						cout << "errrrorr in compare12-2-2";
					}
				}
				else if (ret3 == 1) {	//wff
					int a2[2] = { 0,-1 };
					int b2[2] = { 0,-1 };
					a2[0] = b3[0];
					b2[0] = b3[1];
					int ret4 = balance(a2, b2);
					//b3[0] = b3[1] =f b3[2] = w
					if (ret4 == 2) {
						error_coin.push_back(b3[0]);
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[2]);
					}
					else if (ret4 == 1) {
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[0]);
						error_coin.push_back(b3[2]);
					}
					else if (ret4 == 0) {
						error_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						error_coin.push_back(b3[2]);
					}
					else {
						cout << "errrrorr in compare12-2-0";
					}
				}
				else if (ret3 == 0) { //www
					normal_coin.push_back(b3[0]);
					normal_coin.push_back(b3[1]);
					normal_coin.push_back(b3[2]);
				}

			}
		}
			  break;
			  //앞이 무거움 -> ffffff
		case 1:
			for (int i = 0; i < 6; i++) {
				error_coin.push_back(b[i]);
			}
			break;
			//w 1개 껴있음-> a,b,c로 나누어서
		case 2: {
			int a2[3]; int b2[3]; int c2[3];

			a2[0] = b[0]; a2[1] = b[1]; a2[2] = -1;
			b2[0] = b[2]; b2[1] = b[3]; b2[2] = -1;
			c2[0] = b[4]; c2[1] = b[5]; c2[2] = -1;
			int ret2 = balance(a2, b2);
			//둘다 ff ff였던것
			if (ret2 == 2) {
				for (int i = 0; i < 2; i++) {
					error_coin.push_back(a2[i]);
					error_coin.push_back(b2[i]);
				}	//c비교
				int a1[2]; int b1[2];
				a1[0] = c2[0]; b1[0] = c2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 0, error_coin, normal_coin, same_coin);
			}
			else if (ret2 == 0) {
				for (int i = 0; i < 2; i++) {
					error_coin.push_back(a2[i]);
					error_coin.push_back(c2[i]);
				}
				//b비교
				int a1[2]; int b1[2];
				a1[0] = b2[0]; b1[0] = b2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 0, error_coin, normal_coin, same_coin);
			}
			else if (ret2 == 1) {
				for (int i = 0; i < 2; i++) {
					error_coin.push_back(b2[i]);
					error_coin.push_back(c2[i]);
				}
				//a비교
				int a1[2]; int b1[2];
				a1[0] = a2[0]; b1[0] = a2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 0, error_coin, normal_coin, same_coin);
			}
		}
			  break;
		default:
			printf("error occurs");
			break;
		}
	}
}