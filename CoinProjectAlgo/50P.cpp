#include <stdio.h>
#include "balance.h"
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;
//ERROR, -1 
// 같은 코인 양팔에 올릴 때 
// 동전은 100개

//저울 1 a가크면 LARGE, 1; 
//저울 2 b가크면 SMALL, 0; 같으면 EQUAL, 2

int* at50();
void SeparateSame(vector<vector<int>> samecoin, int round, int size, vector<int>& normal_coin, vector<int>& error_coin);

int* at50() {

	vector<int> error_coin;
	vector<int> normal_coin;
	//100,2짜리 선언
	vector<vector<int>> same_coin;

	int a[2] = { 0,-1 };
	int b[2] = { 1,-1 };
	int i = 0;
	int ret;

	while (i < 100) {
		a[0] = i++;
		b[0] = i++;
		ret = balance(a, b);

		switch (ret)
		{
		case 0: //a에 불량동전
			error_coin.push_back(a[0]);
			normal_coin.push_back(b[0]);
			break;
		case 1://b에 불량동전
			error_coin.push_back(b[0]);
			normal_coin.push_back(a[0]);
			break;
		case 2: {
			vector<int> push;
			push.push_back(a[0]);
			push.push_back(b[0]);
			same_coin.push_back(push);
		}
			  break;
		default:
			printf("error occurs");
			break;
		}
	}
	SeparateSame(same_coin, 2, same_coin.size(), normal_coin, error_coin);

	int size = error_coin.size();
	int* ans = (int*)malloc(sizeof(int) * (size + 1));;
	for (int i = 0; i < size; i++) {
		ans[i] = error_coin[i];
	}
	ans[size] = -1;

	/*sort(ans, ans + size);
	for (int i = 0; i < size; i++) {
		cout << ans[i] << " ";
	}*/


	return ans;
}

//round = samecoin ww wwww wwwwwwww..., size = 같은거 몇개인지.
void SeparateSame(vector<vector<int>> samecoin, int round, int size, vector<int>& normal_coin, vector<int>& error_coin) {

	if (size == 0) return;

	int* a = (int*)malloc(sizeof(int) * round + 1);
	int* b = (int*)malloc(sizeof(int) * round + 1);
	vector<vector<int>> samecoin2;

	int ret = 0;
	a[round] = -1;
	b[round] = -1;

	for (int x = 0; x <= size - 2; x = x + 2) {

		for (int i = 0; i < round; i++) {
			a[i] = samecoin[x][i];
			b[i] = samecoin[x + 1][i];
		}

		cout << a[0] << " " << a[1];
		cout << b[0] << " " << b[1];

		ret = balance(a, b);

		switch (ret)
		{
		case 0: //a에 불량동전
			for (int k = 0; k < round; k++) {
				error_coin.push_back(a[k]);
			}
			for (int k = 0; k < round; k++) {
				normal_coin.push_back(b[k]);
			}
			break;

		case 1://b에 불량동전
			for (int k = 0; k < round; k++) {
				error_coin.push_back(b[k]);
			}
			for (int k = 0; k < round; k++) {
				normal_coin.push_back(a[k]);
			}
			break;

			//둘다 같음
		case 2: {
			vector<int> forsamecoin;
			for (int k = 0; k < round; k++) {
				forsamecoin.push_back(a[k]);
			}
			for (int k = 0; k < round; k++) {
				forsamecoin.push_back(b[k]);
			}
			samecoin2.push_back(forsamecoin);
			break;
		}


		default:
			printf("error occurs");
			break;
		}
	}

	//size가 홀수 -> 마지막에 따로 비교
	if ((size % 2) != 0) {
		int normal[2]; normal[0] = normal_coin.front(); normal[1] = -1;
		int findsame[2]; findsame[1] = -1;

		for (int i = 0; i < round; i++) {
			a[i] = samecoin[size - 1][i];
		}
		findsame[0] = a[0];

		ret = balance(findsame, normal);

		switch (ret)
		{
		case 0: //a에 불량동전
			for (int k = 0; k < round; k++) {
				error_coin.push_back(a[k]);
			}
			break;

		case 1://에러
			printf("error occurs in %d", round);
			break;

		case 2: //둘다 같음
			for (int k = 0; k < round; k++) {
				normal_coin.push_back(a[k]);
			}
			break;
		default:
			printf("error occurs");
			break;
		}
	}

	if (!samecoin2.empty()) {
		//재귀로 호출 (same ww ww ff ff -> wwww ffff -> wwwwwwww ffffffff ....)
		SeparateSame(samecoin2, round * 2, samecoin2.size(), normal_coin, error_coin);
		return;
	}
	return;
}

