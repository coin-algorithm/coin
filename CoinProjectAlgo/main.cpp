#include "balance.h"
//#include "mybalance.h"

// lowerP
#include <vector>
#include <set>
#include <iostream>

using namespace std;

int* multiTwo();
// lowerP

typedef struct Array {
	int arr[100];
	int length = 0;
}c_array;

int *badcoin_less_than_50();
int *badcoin_more_than_50();
void init_array(c_array *arr);
void insert(c_array *arr, int coin);
void insert(c_array *arr, int coin[]);
int compare(int coin1, int coin2);
int compare(int coin1[], int coin2[]);
void two_classifi(c_array *normal_coin, c_array *bad_coin, c_array *same_pair, int num);
void same_pair_classifi(c_array *normal_coin, c_array *bad_coin, c_array same_pair);
void two_item_classifi(c_array *normal_coin, c_array *bad_coin, int num);
void three_normal_classifi(c_array *normal_coin, c_array *bad_coin);
void three_bad_classifi(c_array *normal_coin, c_array *bad_coin);
//bad_coin

int* at10();
int* at90();
int phase1(vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int& index);
int phase2(int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int& index);
int phase3(int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int& index);
void compare2(int ret, int a[2], int b[2], int clue, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin);
void compare4(int ret, int a[5], int b[5], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin);
void compare6(int ret, int a[4], int b[4], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin);
void compare12(int ret, int a[7], int b[7], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin);
//10P, 90P

int* at50();
void SeparateSame(vector<vector<int>> samecoin, int round, int size, vector<int>& normal_coin, vector<int>& error_coin);
//50P



int atoi(char *ch);


int main(int argc, char **argv) {
	/* 불량 동전 초기화 */
	int p = atoi(argv[1]);
	//int p = 50;
	initialize(p);

	/* 정답 배열 */
	int *A=NULL; // 정답 
	int B[] = { -1 };

	/* p에 따른 적용 */
	if (p < 5 || p > 95) {
		A = multiTwo();
	}
	else if (p <= 10) {
		A = at10();
	}
	else if (p < 47) {
		A = badcoin_less_than_50();
	}
	else if (p <= 53) {
		A = at50();
	}
	else if (p < 90) {
		A = badcoin_more_than_50();
	}
	else if (p <= 95) {
		A = at90();
	}
	/* 정답 확인 */
	balance(A, B);
}

int atoi(char *ch) {
	int tmp = 0;
	while (*ch != '\0') {
		tmp = tmp * 10 + *ch - '0';
		ch++;
	}
	return tmp;
}

	// lowerP

vector<int> correct; // 정상동전을 넣어두는 vector
vector<int> uncorrect; // 불량동전을 넣어두는 vector
vector<pair<int, int>> sameTwo; // 같은 2개의 동전을 넣어두는 vector
vector<int>::iterator iter;
vector<pair<int, int>>::iterator p_iter;
int index = 0; // 읽고 있는 동전의 index
set<int> same_set;
set<int>::iterator s_iter;
vector<pair<int, int>> idx_size; // 다른 set이 나왔을 때 idx부터 size만큼을 저장
int* a; int* b;
int flag = -1; // same_set의 동전들이 불량인지 정상인지 판단하는 flag

void compareWithSameSet(int idx, int size) {
	// size를 반으로 줄이면서 어디에 불량동전이 있는지 파악하기
	int* a; int* b;

	a = (int*)malloc(sizeof(int)*(size + 1));
	b = (int*)malloc(sizeof(int)*(size + 1));

	if (size == 2 && correct.size() >= 1 && uncorrect.size() >= 1) {
		// 크기가 2일 때, 정상 및 불량 동전이 최소 1개씩 있으면 '정불'로 비교
		a[0] = correct[0]; a[1] = uncorrect[0]; a[2] = -1;
		b[0] = idx; b[1] = idx + 1; b[2] = -1;
		int res = balance(a, b);

		if (res == EQUAL) { // 불량 1 정상 1
			int tmp[2] = { b[0],b[1] };
			a = (int*)malloc(sizeof(int)*(2));
			b = (int*)malloc(sizeof(int)*(2));
			a[0] = tmp[0]; a[1] = -1;
			b[0] = tmp[1]; b[1] = -1;
			res = balance(a, b);
			if (res == LARGE) {
				correct.push_back(a[0]); uncorrect.push_back(b[0]);
			}
			else {
				correct.push_back(b[0]); uncorrect.push_back(a[0]);

			}
		}
		else if (res == LARGE) { // 둘다 불량동전
			for (int i = 0; i < 2; i++)
				uncorrect.push_back(b[i]);
		}
		else { // 둘다 정상동전
			for (int i = 0; i < 2; i++)
				correct.push_back(b[i]);
		}
	}
	else {
		// 정상, 불량 동전이 하나도 없다면 size를 반으로 줄이면서 1까지 가서 저울에 동전을 하나 올려두고 비교해야함
		// 비효율적이지만 한번만 이와 같이 동작하면 조건이 충족되므로 위의 if문에 들어갈 수 있음
		s_iter = same_set.begin();
		for (int i = 0; i < size; i++) {
			a[i] = *(s_iter++);
			b[i] = idx + i;
		}
		a[size] = -1; b[size] = -1;
		int res = balance(a, b);
		if (res == EQUAL) {
			for (int i = 0; i < size; i++) {
				if (flag)
					correct.push_back(b[i]);
				else
					uncorrect.push_back(b[i]);
			}
		}
		else {
			if (size != 1)
				idx_size.push_back(make_pair(idx, size));
			else {
				if (flag)
					uncorrect.push_back(b[0]);
				else
					correct.push_back(b[0]);
			}
		}
	}
}

int* multiTwo() { // main function
				   // 첫 2개 비교
	int size = 1;
	a = (int*)malloc(sizeof(int)*(size + 1));
	b = (int*)malloc(sizeof(int)*(size + 1));

	a[0] = index; b[0] = index + 1; a[1] = -1; b[1] = -1;
	int res = balance(a, b);

	while (res != EQUAL) { // 2개의 동전이 같을 때까지 // 몇 번 나올지 모름
		if (res == SMALL) {
			uncorrect.push_back(a[0]);
			correct.push_back(b[0]);
			index += 2;
		}
		else if (res == LARGE) {
			correct.push_back(a[0]);
			uncorrect.push_back(b[0]);
			index += 2;
		}
		a[0] = index; b[0] = index + 1; a[1] = -1; b[1] = -1;
		res = balance(a, b);
	}

	same_set.insert(a[0]);
	same_set.insert(b[0]);


	// size 늘려가며 비교
	size *= 2; // size = 2와 같음
	int index2 = index + size; // index2 : 오른쪽 저울에 올릴 미식별 동전의 시작 index

	while (index2 + size < 100) { // 미식별 동전이 충분히 적게 남을때까지 // 최대 12번
		a = (int*)malloc(sizeof(int)*(size + 1));
		b = (int*)malloc(sizeof(int)*(size + 1));

		s_iter = same_set.begin();
		for (int i = 0; i < size; i++) {
			a[i] = *(s_iter++);
			b[i] = index2 + i;
		}
		a[size] = -1; b[size] = -1;
		res = balance(a, b);
		if (res != EQUAL) {
			// res is not equal
			if (res == SMALL)
				flag = 0;
			else if (res == LARGE)
				flag = 1;
			for (int i = 0; i < size; i++)
				same_set.insert(a[i]);
			idx_size.push_back(make_pair(index2, size)); // 다르면 index2와 현재 size를 idx_size에 넣기 => 나중에 처리
			index2 += size;
		}
		else { // 같으면 same_set에 넣기
			for (int i = 0; i < size; i++) {
				same_set.insert(a[i]);
				same_set.insert(b[i]);
			}
			index2 += size;
			// size를 키워가며 같은 동전 비교
			if (size > 8) {
				size = 2;
			}
			size *= 2;
		}
	}

	// 남은 index 처리
	if (100 - index2 > 0) { // 1번
		int s = 100 - index2;
		a = (int*)malloc(sizeof(int)*(s + 1));
		s_iter = same_set.begin();
		for (int i = 0; i < s; i++) {
			a[i] = *(s_iter++);
		}
		b = (int*)malloc(sizeof(int)*(s + 1));
		for (int i = 0; i < s; i++)
			b[i] = index2 + i;
		a[s] = -1; b[s] = -1;
		int res = balance(a, b);
		if (res == EQUAL) {
			for (int i = 0; i < s; i++) {
				same_set.insert(b[i]);
			}
		}
		else {
			if (res == SMALL) {
				flag = 0;
			}
			else if (res == LARGE) {
				flag = 1;
			}
			idx_size.push_back(make_pair(index2, s));
		}
	}

	// flag가 -1일 때 same_set 처리
	if (flag == -1) {
		int res;
		a = (int*)malloc(sizeof(int) * 2);
		b = (int*)malloc(sizeof(int) * 2);
		if (correct.size() > 0) {
			a[0] = correct[0]; a[1] = -1;
			s_iter = same_set.begin();
			b[0] = *s_iter; b[1] = -1;
			res = balance(a, b);
			if (res == LARGE)
				flag = 0;
			else
				flag = 1;
		}
		else if (uncorrect.size()>0) {
			a[0] = uncorrect[0]; a[1] = -1;
			s_iter = same_set.begin();
			b[0] = *s_iter; b[1] = -1;
			res = balance(a, b);
			if (res == SMALL)
				flag = 1;
			else
				flag = 0;
		}
	}

	// same_set 처리
	for (s_iter = same_set.begin(); s_iter != same_set.end(); s_iter++) {
		if (flag) // 판단해둔 flag에 따라 처리
			correct.push_back(*s_iter);
		else
			uncorrect.push_back(*s_iter);
	}

	// idx_size 처리
	while (idx_size.size()) { // 최대 84번 => p가 중간일 때
		auto tmp = idx_size.back();
		idx_size.pop_back();
		int t_idx = tmp.first; int t_size = tmp.second;

		// size를 반으로 쪼개서 앞의 동전들 비교
		t_size /= 2;
		compareWithSameSet(t_idx, t_size); // 최대 14번

										   // size를 반으로 쪼개서 뒤의 동전들 비교
		t_idx += t_size;
		t_size = tmp.second - t_size;
		compareWithSameSet(t_idx, t_size);
	}

	// 불량동전 넣기
	a = (int*)malloc(sizeof(int)*(uncorrect.size() + 1));
	int u_size = uncorrect.size();
	for (int i = 0; i < u_size; i++) {
		a[i] = uncorrect[i];
		cout << uncorrect[i] << " ";
	}
	cout << endl;
	a[u_size] = -1;
	return a;
}
	//lowerP




int *badcoin_less_than_50() {

	int *A;

	c_array normal_coin;
	c_array bad_coin;
	c_array same_pair;
	//init_array(&normal_coin);
	//init_array(&bad_coin);
	//init_array(&same_pair);

	/*2개씩 묶어 비교*/
	for (int i = 0; (i < 100) && (bad_coin.length == 0) && (normal_coin.length == 0); i += 4) {
		two_classifi(&normal_coin, &bad_coin, &same_pair, i);
	}

	/*same_pair로 판단된 것들 분류*/
	same_pair_classifi(&normal_coin, &bad_coin, same_pair);

	// p <= 50
	for (int i = bad_coin.length + normal_coin.length; i < 98 && normal_coin.length < 2; i += 2) {
		two_item_classifi(&normal_coin, &bad_coin, i);
	}
	while (bad_coin.length + normal_coin.length < 98)
		three_normal_classifi(&normal_coin, &bad_coin);

	/*남은동전세기*/

	if (bad_coin.length + normal_coin.length <= 98) {	//2개 남았을경우
		two_item_classifi(&normal_coin, &bad_coin, bad_coin.length + normal_coin.length);
	}
	if (bad_coin.length + normal_coin.length == 99) {	//1개만 남았을경우
		int ret = compare(normal_coin.arr[0], 99);
		if (ret == 1) {
			insert(&bad_coin, 99);
		}
		else if (ret == 2) {
			insert(&normal_coin, 99);
		}
	}

	A = (int*)malloc(sizeof(int)*(bad_coin.length + 1));
	for (int i = 0; i < bad_coin.length; i++) {
		A[i] = bad_coin.arr[i];
	}
	A[bad_coin.length] = -1;

	return A;
}
int *badcoin_more_than_50() {

	int *A;

	c_array normal_coin;
	c_array bad_coin;
	c_array same_pair;
	init_array(&normal_coin);
	init_array(&bad_coin);
	init_array(&same_pair);


	/*2개씩 묶어 비교*/
	for (int i = 0; (i < 100) && (bad_coin.length == 0) && (normal_coin.length == 0); i += 4) {
		two_classifi(&normal_coin, &bad_coin, &same_pair, i);
	}

	/*same_pair로 판단된 것들 분류*/
	same_pair_classifi(&normal_coin, &bad_coin, same_pair);


	// p > 50
	for (int i = bad_coin.length + normal_coin.length; i < 98 && bad_coin.length < 2; i += 2) {
		two_item_classifi(&normal_coin, &bad_coin, i);
	}
	while (bad_coin.length + normal_coin.length < 98)
		three_bad_classifi(&normal_coin, &bad_coin);



	/*남은동전세기*/

	if (bad_coin.length + normal_coin.length <= 98) {	//2개 남았을경우
		two_item_classifi(&normal_coin, &bad_coin, bad_coin.length + normal_coin.length);
	}
	if (bad_coin.length + normal_coin.length == 99) {	//1개만 남았을경우
		int ret = compare(normal_coin.arr[0], 99);
		if (ret == 1) {
			insert(&bad_coin, 99);
		}
		else if (ret == 2) {
			insert(&normal_coin, 99);
		}
	}

	A = (int*)malloc(sizeof(int)*(bad_coin.length + 1));
	for (int i = 0; i < bad_coin.length; i++) {
		A[i] = bad_coin.arr[i];
	}
	A[bad_coin.length] = -1;

	return A;
}
void init_array(c_array *arr) {
	for (int i = 0; i < 100; i++) {
		arr->arr[i] = -1;
	}
	arr->length = 0;
}
void insert(c_array *arr, int coin) {
	arr->arr[arr->length] = coin;
	arr->length += 1;
}
void insert(c_array *arr, int coin[]) { //모두 같은 동전일경우
	for (int i = 0; coin[i] != -1; i++) {
		arr->arr[arr->length] = coin[i];
		arr->length += 1;
	}
}
int compare(int coin1, int coin2) {

	int arr1[] = { coin1,-1 };
	int arr2[] = { coin2,-1 };

	return balance(arr1, arr2);

}
int compare(int coin1[], int coin2[]) {
	return balance(coin1, coin2);
}
void two_classifi(c_array *normal_coin, c_array *bad_coin, c_array *same_pair, int num) {

	int a[3] = { num,num + 1,-1 };
	int b[3] = { num + 2,num + 3,-1 };
	int ret = balance(a, b);

	if (ret == 2) {	//무게가 같을경우
		insert(same_pair, a);
		insert(same_pair, b);
	}
	else if (ret == 0) { //a<b인경우
		int ret2 = compare(a[0], a[1]);

		if (ret2 == 2) { //a 둘 다 불량동전
			insert(bad_coin, a);

			int ret3 = compare(b[0], b[1]);
			if (ret3 == 2) {	//b 둘다 정상동전
				insert(normal_coin, b);
			}
			else if (ret3 == 0) { //b[0] 불량동전 b[1] 정상동전
				insert(bad_coin, b[0]);
				insert(normal_coin, b[1]);
			}
			else if (ret3 == 1) {//b[0] 정상동전 b[1] 불량동전
				insert(normal_coin, b[0]);
				insert(bad_coin, b[1]);
			}
		}
		else {// 불량하나 정상하나일 경우
			insert(normal_coin, b); //b는 모두 정상동전

			if (ret2 == 0) {	//a[0]은 불량동전 a[1]은 정상동전
				insert(bad_coin, a[0]);
				insert(normal_coin, a[1]);
			}
			else if (ret2 == 1) { //a[0]은 정상동전, a[1]은 불량동전
				insert(normal_coin, a[0]);
				insert(bad_coin, a[1]);
			}
		}
	}
	else if (ret == 1) { //a>b인경우
		int ret2 = compare(b[0], b[1]);

		if (ret2 == 2) { //b 둘 다 불량동전
			insert(bad_coin, b);

			int ret3 = compare(a[0], a[1]);
			if (ret3 == 2) {	//a 둘다 정상동전
				insert(normal_coin, a);
			}
			else if (ret3 == 0) { //a[0] 불량동전 a[1] 정상동전
				insert(bad_coin, a[0]);
				insert(normal_coin, a[1]);
			}
			else if (ret3 == 1) {//a[0] 정상동전 a[1] 불량동전
				insert(normal_coin, a[0]);
				insert(bad_coin, a[1]);
			}
		}
		else {// 불량하나 정상하나일 경우
			insert(normal_coin, a); //a는 모두 정상동전

			if (ret2 == 0) {	//b[0]은 불량동전 b[1]은 정상동전
				insert(bad_coin, b[0]);
				insert(normal_coin, b[1]);
			}
			else if (ret2 == 1) { //b[0]은 정상동전, b[1]은 불량동전
				insert(normal_coin, b[0]);
				insert(bad_coin, b[1]);
			}
		}
	}
}
void same_pair_classifi(c_array *normal_coin, c_array *bad_coin, c_array same_pair) {

	int compare_arr1[] = { normal_coin->arr[0],bad_coin->arr[0],-1 };

	for (int i = 0; i < same_pair.length; i += 4) {
		int arr[] = { same_pair.arr[i],same_pair.arr[i + 1],-1 };
		int ret = compare(compare_arr1, arr);

		if (ret == 0) {	//모두 정상동전
			insert(normal_coin, arr);
			insert(normal_coin, same_pair.arr[i + 2]);
			insert(normal_coin, same_pair.arr[i + 3]);

		}
		else if (ret == 1) { //모두 불량동전
			insert(bad_coin, arr);
			insert(bad_coin, same_pair.arr[i + 2]);
			insert(bad_coin, same_pair.arr[i + 3]);
		}
		else if (ret == 2) {//하나는 불량 하나는 정상
			int ret2 = compare(same_pair.arr[i], same_pair.arr[i + 1]);
			if (ret2 == 0) {
				insert(bad_coin, same_pair.arr[i]);
				insert(normal_coin, same_pair.arr[i + 1]);
			}
			else if (ret2 == 1) {//arr[0]정상 arr[1]불량
				insert(normal_coin, same_pair.arr[i]);
				insert(bad_coin, same_pair.arr[i + 1]);
			}

			int ret3 = compare(same_pair.arr[i + 2], same_pair.arr[i + 3]);
			if (ret3 == 0) {
				insert(bad_coin, same_pair.arr[i + 2]);
				insert(normal_coin, same_pair.arr[i + 3]);
			}
			else if (ret3 == 1) {//arr[0]정상 arr[1]불량
				insert(normal_coin, same_pair.arr[i + 2]);
				insert(bad_coin, same_pair.arr[i + 3]);
			}
		}
	}

}
void two_item_classifi(c_array *normal_coin, c_array *bad_coin, int num) {
	int compare_arr[] = { normal_coin->arr[0],bad_coin->arr[0],-1 };
	int a[] = { num,num + 1,-1 };

	int ret = compare(compare_arr, a);

	if (ret == 0) {
		insert(normal_coin, a);
	}
	else if (ret == 1) {
		insert(bad_coin, a);
	}
	else if (ret == 2) {
		int ret2 = compare(num, num + 1);

		if (ret2 == 0) {
			insert(bad_coin, num);
			insert(normal_coin, num + 1);
		}
		else if (ret2 == 1) {
			insert(normal_coin, num);
			insert(bad_coin, num + 1);
		}
	}
}
void three_normal_classifi(c_array *normal_coin, c_array *bad_coin) { //정상코인의 개수가 더 많을경우
	int compare_arr2[] = { normal_coin->arr[0],normal_coin->arr[1],bad_coin->arr[0],-1 };

	int i = bad_coin->length + normal_coin->length;
	int arr[] = { i,i + 1,i + 2,-1 };
	int ret = compare(compare_arr2, arr);

	if (ret == 0) {// 모두다 정상
		insert(normal_coin, arr);
	}
	else if (ret == 1) {	//정상1 불량2
		int ret2 = compare(i, i + 1);
		if (ret2 == 0) {
			insert(bad_coin, i);
			insert(normal_coin, i + 1);
			insert(bad_coin, i + 2);
		}
		else if (ret2 == 1) {
			insert(normal_coin, i);
			insert(bad_coin, i + 1);
			insert(bad_coin, i + 2);
		}
		else if (ret2 == 2) {	//불량2 정상1 or 불량3
			int ret3 = compare(i + 1, i + 2);
			if (ret3 == 0) {
				insert(bad_coin, i);
				insert(bad_coin, i + 1);
				insert(normal_coin, i + 2);
			}
			else if (ret3 == 2) {
				insert(bad_coin, i);
				insert(bad_coin, i + 1);
				insert(bad_coin, i + 2);
				ret = 3;	//six_classifi에서 사용하기 위해 return값 조정....(다른방법없나ㅠ)
			}
		}
	}
	else if (ret == 2) {
		int ret2 = compare(i, i + 1);
		if (ret2 == 0) {
			insert(bad_coin, i);
			insert(normal_coin, i + 1);
			insert(normal_coin, i + 2);
		}
		else if (ret2 == 1) {
			insert(normal_coin, i);
			insert(bad_coin, i + 1);
			insert(normal_coin, i + 2);
		}
		else if (ret2 == 2) {
			insert(normal_coin, i);
			insert(normal_coin, i + 1);
			insert(bad_coin, i + 2);
		}
	}
}
void three_bad_classifi(c_array *normal_coin, c_array *bad_coin) {	//불량동전의 개수가 더 많을경우
	int compare_arr2[] = { normal_coin->arr[0],bad_coin->arr[0],bad_coin->arr[1],-1 };

	int i = bad_coin->length + normal_coin->length;
	int arr[] = { i,i + 1,i + 2,-1 };

	int ret = compare(compare_arr2, arr);

	if (ret == 0) { //정상2,불량1 or 정상3
		int ret2 = compare(i, i + 1);

		if (ret2 == 0) {
			insert(bad_coin, i);
			insert(normal_coin, i + 1);
			insert(normal_coin, i + 2);
		}
		else if (ret2 == 1) {
			insert(normal_coin, i);
			insert(bad_coin, i + 1);
			insert(normal_coin, i + 2);
		}
		else if (ret2 == 2) {
			int ret3 = compare(i + 1, i + 2);

			if (ret3 == 1) {
				insert(normal_coin, i);
				insert(normal_coin, i + 1);
				insert(bad_coin, i + 2);
			}
			else if (ret3 == 2) {
				insert(normal_coin, i);
				insert(normal_coin, i + 1);
				insert(normal_coin, i + 2);
			}
		}
	}
	else if (ret == 1) {
		insert(bad_coin, arr);
	}
	else if (ret == 2) {//정상1 불량2
		int ret2 = compare(i, i + 1);

		if (ret2 == 0) {
			insert(bad_coin, i);
			insert(normal_coin, i + 1);
			insert(bad_coin, i + 2);
		}
		else if (ret2 == 1) {
			insert(normal_coin, i);
			insert(bad_coin, i + 1);
			insert(bad_coin, i + 2);
		}
		else if (ret2 == 2) {
			insert(bad_coin, i);
			insert(bad_coin, i + 1);
			insert(normal_coin, i + 2);
		}

	}
}
	//bad_coin



int* at10()
{
	vector<int> error_coin;
	vector<int> normal_coin;
	//100,2짜리 선언
	vector<int> same_coin;
	int index = 0;
	int wwf[4] = { -1,-1,-1,-1 };

	while (error_coin.size() < 1) {

		if (phase1(error_coin, normal_coin, same_coin, index) == 2) {
			if (phase2(1, error_coin, normal_coin, same_coin, index) == 2) {
				phase3(1, error_coin, normal_coin, same_coin, index);
			}
		}

		//만약 불량동전이 0번이나 1번이라 1,1개씩 밖에 못만들면
		while (normal_coin.size() < 2) {
			int a[2];
			int b[2];
			a[0] = error_coin[0]; a[1] = -1;
			b[0] = index++; b[1] = -1;
			int ret = balance(a, b);
			compare2(ret, a, b, 3, error_coin, normal_coin, same_coin);
		}
	}

	wwf[0] = normal_coin[0];
	wwf[1] = normal_coin[1];
	wwf[2] = error_coin[0];

	//wwf로 비교 -> w 5개 모을때까지만.
	if (normal_coin.size() < 5) {
		for (; index < 98; index = index + 3) {
			int a4[4]; a4[0] = index; a4[1] = index + 1; a4[2] = index + 2; a4[3] = -1;
			int ret = balance(wwf, a4);
			compare6(ret, wwf, a4, 1, error_coin, normal_coin, same_coin);
			if (normal_coin.size() > 5) {
				index = index + 3;
				break;
			}
		}
	}

	//wwwwwf로 비교
	for (; index < 95; index = index + 6) {
		//cout << index << " " << index + 5 << "\n";
		int a7[7]; a7[0] = index; a7[1] = index + 1; a7[2] = index + 2; a7[3] = index + 3; a7[4] = index + 4; a7[5] = index + 5; a7[6] = -1;
		int wwwwwf[7]; wwwwwf[0] = normal_coin[0]; wwwwwf[1] = normal_coin[1]; wwwwwf[2] = normal_coin[2]; wwwwwf[3] = normal_coin[3]; wwwwwf[4] = normal_coin[4];
		wwwwwf[5] = error_coin[0]; wwwwwf[6] = -1;
		int ret = balance(wwwwwf, a7);
		compare12(ret, wwwwwf, a7, 1, error_coin, normal_coin, same_coin);
	}

	//마무리작업
	for (; index < 98; index = index + 3) {
		int a4[4]; a4[0] = index; a4[1] = index + 1; a4[2] = index + 2; a4[3] = -1;
		int ret = balance(wwf, a4);
		compare6(ret, wwf, a4, 1, error_coin, normal_coin, same_coin);
	}
	if (index == 98) {
		int a[3]; a[0] = normal_coin[0]; a[1] = error_coin[1]; a[2] = -1;
		int b[3]; b[0] = index; b[1] = index + 1; b[2] = -1;
		int ret = balance(a, b);

		switch (ret)
		{
			//뒤에가 무거움 = ww
		case 0:
			normal_coin.push_back(b[0]);
			normal_coin.push_back(b[1]);
			break;

			//뒤에가 가벼움 = ff
		case 1:
			error_coin.push_back(b[0]);
			error_coin.push_back(b[1]);
			break;

			//둘다 wf
		case 2: {
			int a1[2];
			int b1[2];
			a1[0] = b[0]; a1[1] = -1;
			b1[0] = b[1]; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 2, error_coin, normal_coin, same_coin);
			break;
		}
		default:
			"compare2 errors";
			break;
		}
	}
	else if (index == 99) {
		int a[2];
		int b[2];
		a[0] = error_coin[0]; a[1] = -1;
		b[0] = index++; b[1] = -1;
		int ret = balance(a, b);
		compare2(ret, a, b, 3, error_coin, normal_coin, same_coin);
	}

	int size = error_coin.size();
	int* ans = (int*)malloc(sizeof(int) * (size + 1));;
	for (int i = 0; i < size; i++) {
		ans[i] = error_coin[i];
	}
	ans[size] = -1;

	/*for (int i = 0; i < error_coin.size(); i++) {
		cout << error_coin[i] << " ";
	}
	cout << "\n ";
	for (int i = 0; i < normal_coin.size(); i++) {
		cout << normal_coin[i] << " ";
	}*/
	return ans;
}
int* at90()
{
	vector<int> error_coin;
	vector<int> normal_coin;
	//100,2짜리 선언
	vector<int> same_coin;
	int index = 0;
	int wff[4] = { -1,-1,-1,-1 };

	while (normal_coin.size() < 1) {

		if (phase1(error_coin, normal_coin, same_coin, index) == 2) {
			if (phase2(2, error_coin, normal_coin, same_coin, index) == 2) {
				phase3(2, error_coin, normal_coin, same_coin, index);
			}
		}

		//만약 정상동전이 0번이나 1번이라 1,1개씩 밖에 못만들면
		while (error_coin.size() < 2) {
			int a[2];
			int b[2];
			a[0] = error_coin[0]; a[1] = -1;
			b[0] = index++; b[1] = -1;
			int ret = balance(a, b);
			compare2(ret, a, b, 3, error_coin, normal_coin, same_coin);
		}
	}

	wff[0] = normal_coin[0];
	wff[1] = error_coin[1];
	wff[2] = error_coin[0];

	//wff로 비교 -> f 5개 모을때까지만.
	if (error_coin.size() < 5) {
		for (; index < 98; index = index + 3) {
			int a4[4]; a4[0] = index; a4[1] = index + 1; a4[2] = index + 2; a4[3] = -1;
			int ret = balance(wff, a4);
			compare6(ret, wff, a4, 2, error_coin, normal_coin, same_coin);
			if (error_coin.size() > 5) {
				index = index + 3;
				break;
			}
		}
	}

	//wfffff로 비교
	for (; index < 95; index = index + 6) {
		//cout << index << " " << index + 5 << "\n";
		int a7[7]; a7[0] = index; a7[1] = index + 1; a7[2] = index + 2; a7[3] = index + 3; a7[4] = index + 4; a7[5] = index + 5; a7[6] = -1;
		int wfffff[7]; wfffff[0] = error_coin[0]; wfffff[1] = error_coin[1]; wfffff[2] = error_coin[2]; wfffff[3] = error_coin[3]; wfffff[4] = error_coin[4];
		wfffff[5] = normal_coin[0]; wfffff[6] = -1;
		int ret = balance(wfffff, a7);
		compare12(ret, wfffff, a7, 2, error_coin, normal_coin, same_coin);
	}

	wff[0] = normal_coin[0];
	wff[1] = error_coin[1];
	wff[2] = error_coin[0];

	//마무리작업
	for (; index < 98; index = index + 3) {
		int a4[4]; a4[0] = index; a4[1] = index + 1; a4[2] = index + 2; a4[3] = -1;
		int ret = balance(wff, a4);
		compare6(ret, wff, a4, 2, error_coin, normal_coin, same_coin);
	}
	if (index == 98) {
		int a[3]; a[0] = normal_coin[0]; a[1] = error_coin[1]; a[2] = -1;
		int b[3]; b[0] = index; b[1] = index + 1; b[2] = -1;
		int ret = balance(a, b);

		switch (ret)
		{
			//뒤에가 무거움 = ww
		case 0:
			normal_coin.push_back(b[0]);
			normal_coin.push_back(b[1]);
			break;

			//뒤에가 가벼움 = ff
		case 1:
			error_coin.push_back(b[0]);
			error_coin.push_back(b[1]);
			break;

			//둘다 wf
		case 2: {
			int a1[2];
			int b1[2];
			a1[0] = b[0]; a1[1] = -1;
			b1[0] = b[1]; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 2, error_coin, normal_coin, same_coin);
			break;
		}
		default:
			"compare2 errors";
			break;
		}
	}
	else if (index == 99) {
		int a[2];
		int b[2];
		a[0] = error_coin[0]; a[1] = -1;
		b[0] = index++; b[1] = -1;
		int ret = balance(a, b);
		compare2(ret, a, b, 3, error_coin, normal_coin, same_coin);
	}

	int size = error_coin.size();
	int* ans = (int*)malloc(sizeof(int) * (size + 1));;
	for (int i = 0; i < size; i++) {
		ans[i] = error_coin[i];
	}
	ans[size] = -1;
	return ans;
}
int phase1(vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int& index) {

	int a[2];
	int b[2];
	a[0] = index++; a[1] = -1;
	b[0] = index++; b[1] = -1;
	int ret = balance(a, b);
	compare2(ret, a, b, 2, error_coin, normal_coin, same_coin);
	return ret;
}
int phase2(int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int& index) {
	int a[3];
	int b[3];
	int ret = 0;
	if (ver == 1) {
		for (int i = 0; i < 2; i++) {
			a[i] = same_coin[i];
		}
		for (int i = 0; i < 2; i++) {
			b[i] = index++;
		}
		a[2] = -1; b[2] = -1;
		same_coin.clear();
		ret = balance(a, b);

		switch (ret)
		{
		case 0: {//a에 불량동전이었던거임 -> b는 ww or wf
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 1, error_coin, normal_coin, same_coin);

			for (int i = 0; i < 2; i++) {
				error_coin.push_back(a[i]);
			}
		}
			  break;
		case 1: { //b에 불량동전  wf or ff
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 0, error_coin, normal_coin, same_coin);

			for (int i = 0; i < 2; i++) {
				normal_coin.push_back(a[i]);
			}
		}
			  break;
		case 2: {
			for (int i = 0; i < 2; i++) {
				same_coin.push_back(a[i]);
				same_coin.push_back(b[i]);
			}
		}
			  break;
		default:
			printf("error occurs in p2");
			break;
		}
	}
	else if (ver == 2) {
		for (int i = 0; i < 2; i++) {
			a[i] = same_coin[i];
		}
		for (int i = 0; i < 2; i++) {
			b[i] = index++;
		}
		a[2] = -1; b[2] = -1;
		same_coin.clear();
		ret = balance(a, b);

		switch (ret)
		{
		case 0: //b에 정상동전 존재

		{ //b에 정상동전  wf or ww
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 1, error_coin, normal_coin, same_coin);

			for (int i = 0; i < 2; i++) {
				error_coin.push_back(a[i]);
			}
		}

		break;
		//a가 모두 정상동전이었던거임. -> b는 ffor wf
		case 1:
		{
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 0, error_coin, normal_coin, same_coin);
			for (int i = 0; i < 2; i++) {
				normal_coin.push_back(a[i]);
			}
		}

		break;
		case 2: { //진짜 극악의 확률로 a = b = wwww
			for (int i = 0; i < 2; i++) {
				same_coin.push_back(a[i]);
				same_coin.push_back(b[i]);
			}
		}
			  break;
		default:
			printf("error occurs in p2");
			break;
		}
	}
	return ret;
}
int phase3(int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin, int& index) {
	int a[5];
	int b[5];
	int ret = 0;
	if (ver == 1) {
		for (int i = 0; i < 4; i++) {
			a[i] = same_coin[i];
		}
		for (int i = 0; i < 4; i++) {
			b[i] = index++;
		}
		a[4] = -1; b[4] = -1;
		ret = balance(a, b);

		switch (ret)
		{
			//a가 모두 불량동전이었던거임.
		case 0: {
			for (int i = 0; i < 4; i++) {
				error_coin.push_back(a[i]);
			}
			//b에 정상동전 (b4개 비교) -> ffff 1234 -> ff와 12 먼저 비교
			int a1[3]; a1[0] = a[0]; a1[1] = a[1]; a1[2] = -1;
			int b1[3]; b1[0] = b[0]; b1[1] = b[1]; b1[2] = -1;
			int ret2 = balance(a1, b1);
			compare4(ret2, a, b, 2, error_coin, normal_coin, same_coin);
			break;
		}


		case 1: {//b에 불량동전 (b4개 비교) -> wwww 1234 -> ww와 12 먼저 비교
			int a1[3]; a1[0] = a[0]; a1[1] = a[1]; a1[2] = -1;
			int b1[3]; b1[0] = b[0]; b1[1] = b[1]; b1[2] = -1;
			int ret2 = balance(a1, b1);
			compare4(ret2, a, b, 1, error_coin, normal_coin, same_coin);
			break;
		}

		case 2: {
			for (int i = 0; i < 4; i++) {
				same_coin.push_back(b[i]);
			}
		}
			  break;
		default:
			printf("error occurs in p3");
			break;
		}
		//8개가 다 F일 확률은 극히 낮음.
		for (int i = 0; i < same_coin.size(); i++) {
			normal_coin.push_back(same_coin[i]);
		}
		same_coin.clear();
	}
	else if (ver == 2) {
		for (int i = 0; i < 4; i++) {
			a[i] = same_coin[i];
		}
		for (int i = 0; i < 4; i++) {
			b[i] = index++;
		}
		a[4] = -1; b[4] = -1;
		ret = balance(a, b);

		switch (ret)
		{
		case 0: { //b에 정상동전 (b4개 비교) -> ffff 1234 -> ff와 12 먼저 비교
			int a1[3]; a1[0] = a[0]; a1[1] = a[1]; a1[2] = -1;
			int b1[3]; b1[0] = b[0]; b1[1] = b[1]; b1[2] = -1;
			int ret2 = balance(a1, b1);
			compare4(ret2, a, b, 2, error_coin, normal_coin, same_coin);
		}
			  break;

		case 1: {//a가 모두 정상동전이었음.
			for (int i = 0; i < 4; i++) {
				error_coin.push_back(a[i]);
			} //b에 불량동전 wwww 1234 ww와 12먼저 비교
			int a1[3]; a1[0] = a[0]; a1[1] = a[1]; a1[2] = -1;
			int b1[3]; b1[0] = b[0]; b1[1] = b[1]; b1[2] = -1;
			int ret2 = balance(a1, b1);
			compare4(ret2, a, b, 1, error_coin, normal_coin, same_coin);
		}
			  break;
		case 2: {
			for (int i = 0; i < 4; i++) {
				same_coin.push_back(b[i]);
			}
		}
			  break;
		default:
			printf("error occurs in p3");
			break;
		}
		//8개가 다 W일 확률은 극히 낮음.
		for (int i = 0; i < same_coin.size(); i++) {
			error_coin.push_back(same_coin[i]);
		}
		same_coin.clear();
	}
	return ret;
}
void compare2(int ret, int a[2], int b[2], int clue, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin) {

	//ff or fw이다.
	if (clue == 0) {
		switch (ret)
		{
			//앞에가 가벼움
		case 0:
			error_coin.push_back(a[0]);
			normal_coin.push_back(b[0]);
			break;

		case 1:
			error_coin.push_back(b[0]);
			normal_coin.push_back(a[0]);
			break;
		case 2:
			error_coin.push_back(a[0]);
			error_coin.push_back(b[0]);
			break;
		default:
			printf("error occurs");
			break;
		}
	}

	//ww or fw이다.
	else if (clue == 1) {

		switch (ret)
		{
			//앞에가 가벼움
		case 0:
			error_coin.push_back(a[0]);
			normal_coin.push_back(b[0]);
			break;

			//뒤에가 가벼움
		case 1:
			error_coin.push_back(b[0]);
			normal_coin.push_back(a[0]);
			break;

		case 2:
			normal_coin.push_back(a[0]);
			normal_coin.push_back(b[0]);
			break;
		default:
			printf("error occurs");
			break;
		}
	}
	//a는 error인 기준이니깐 넣지마라
	else if (clue == 3) {
		switch (ret)
		{
			//앞에가 가벼움
		case 0:
			normal_coin.push_back(b[0]);
			break;

			//뒤에가 가벼움
		case 1:
			error_coin.push_back(b[0]);
			break;

		case 2:
			error_coin.push_back(b[0]);
			break;
		default:
			printf("error occurs");
			break;
		}
	}
	else {
		switch (ret)
		{
			//앞에가 가벼움
		case 0:
			error_coin.push_back(a[0]);
			normal_coin.push_back(b[0]);
			break;
		case 1:
			error_coin.push_back(b[0]);
			normal_coin.push_back(a[0]);
			break;
		case 2:
			same_coin.push_back(a[0]);
			same_coin.push_back(b[0]);
			break;
		default:
			printf("error occurs");
			break;
		}
	}
}
void compare4(int ret, int a[5], int b[5], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin) {
	//w가 많을때
	if (ver == 1) {
		switch (ret)
		{
			//앞에가 가벼움
		case 0:
			printf("error occurs in c4_0");
			break;

			//앞에가 무거움
		case 1: {//그럼 0,1은 wf or ff
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 0, error_coin, normal_coin, same_coin);

			//뒤에 2,3은 ww or wf or ff
			int a2[3]; a2[0] = error_coin[0]; a2[1] = normal_coin[0]; a2[2] = -1;
			int b2[3]; b2[0] = b[2]; b2[1] = b[3]; b2[2] = -1;
			ret2 = balance(a2, b2);

			switch (ret2)
			{
				//뒤에가 무거움 = ww
			case 0:
				normal_coin.push_back(b2[0]);
				normal_coin.push_back(b2[1]);
				break;

				//앞에가 가벼움 = ff
			case 1:
				error_coin.push_back(b2[0]);
				error_coin.push_back(b2[1]);
				break;

				//둘다 wf
			case 2: {
				a1[0] = b2[0]; b1[0] = b2[1];
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 0, error_coin, normal_coin, same_coin);
				break;
			}
			default:
				"compare2 errors";
				break;
			}
		}
			  break;
			  //얘네 둘도 정상
		case 2: {
			normal_coin.push_back(b[0]);
			normal_coin.push_back(b[1]);

			//그럼 뒤에 애는 wf 아니면 ff
			int a1[2]; int b1[2];
			a1[0] = b[2]; b1[0] = b[3]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 0, error_coin, normal_coin, same_coin);
		}

			  break;
		default:
			"compare2 errors";
			break;
		}
	}
	//f가 많을때
	else if (ver == 2) {
		switch (ret)
		{
			//앞에가 가벼움
		case 0: { //그럼 0,1은 wf or ww
			int a1[2]; int b1[2];
			a1[0] = b[0]; b1[0] = b[1]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 1, error_coin, normal_coin, same_coin);

			//뒤에 2,3은 ww or wf or ff
			int a2[3]; a2[0] = error_coin[0]; a2[1] = normal_coin[0]; a2[2] = -1;
			int b2[3]; b2[0] = b[2]; b2[1] = b[3]; b2[2] = -1;
			ret2 = balance(a2, b2);

			switch (ret2)
			{
				//뒤에가 무거움 = ww
			case 0:
				normal_coin.push_back(b2[0]);
				normal_coin.push_back(b2[1]);
				break;

				//앞에가 가벼움 = ff
			case 1:
				error_coin.push_back(b2[0]);
				error_coin.push_back(b2[1]);
				break;

				//둘다 wf
			case 2: {
				a1[0] = b2[0]; b1[0] = b2[1];
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 0, error_coin, normal_coin, same_coin);
				break;
			}
			default:
				"compare2 errors";
				break;
			}

		}
			  break;
			  //앞에가 무거움
		case 1: {
			printf("error occurs in c4_1");
			break;
		}
			  break;
			  //얘네 둘도 불량
		case 2: {
			error_coin.push_back(b[0]);
			error_coin.push_back(b[1]);

			//그럼 뒤에 애는 wf 아니면 ww
			int a1[2]; int b1[2];
			a1[0] = b[2]; b1[0] = b[3]; a1[1] = -1; b1[1] = -1;
			int ret2 = balance(a1, b1);
			compare2(ret2, a1, b1, 1, error_coin, normal_coin, same_coin);
		}
			  break;
		default:
			"compare2 errors";
			break;
		}
	}


}
void compare6(int ret, int a[4], int b[4], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin) {
	int ret2 = 0;
	int a2[2] = { 0,-1 };
	int b2[2] = { 0,-1 };
	a2[0] = b[0];
	b2[0] = b[1];

	//ver 1 = wwf이다.
	if (ver == 1) {
		switch (ret)
		{
			//뒤에가 무거움 = www
		case 0: {
			normal_coin.push_back(b[0]);
			normal_coin.push_back(b[1]);
			normal_coin.push_back(b[2]);
		}
			  break;

			  //뒤에가 가벼운 = wff / fff
		case 1:
			ret2 = balance(a2, b2);

			//둘이 같음 = FF -> a[2] = w or f
			if (ret2 == 2) {
				error_coin.push_back(b[0]);
				a2[0] = b[2];
				int ret3 = balance(a2, b2);
				compare2(ret3, a2, b2, 0, error_coin, normal_coin, same_coin);
			}
			//a[0] = w, a[1] = f, a[2] = f
			else if (ret2 == 1) {
				error_coin.push_back(b[2]);
				normal_coin.push_back(b[0]);
				error_coin.push_back(b[1]);
			}
			//b[0] = f, b[1] = w, b[2] = f
			else if (ret2 == 0) {
				error_coin.push_back(b[2]);
				normal_coin.push_back(b[1]);
				error_coin.push_back(b[0]);
			}
			break;

			//얘도 wwf
		case 2:
			ret2 = balance(a2, b2);
			//b[0] = b[1] =w b[2] = f
			if (ret2 == 2) {
				normal_coin.push_back(b[0]);
				normal_coin.push_back(b[1]);
				error_coin.push_back(b[2]);
			}
			else if (ret2 == 1) {
				error_coin.push_back(b[1]);
				normal_coin.push_back(b[0]);
				normal_coin.push_back(b[2]);
			}
			else if (ret2 == 0) {
				error_coin.push_back(b[0]);
				normal_coin.push_back(b[1]);
				normal_coin.push_back(b[2]);
			}
			else {
				cout << "errrrorr in compare6-2";
			}
			break;
		default:
			cout << "errrrorr in compare6-1";
			break;
		}
	}
	//wff이다
	else if (ver == 2) {
		switch (ret)
		{
			//뒤에가 무거움 = wwf, www
		case 0: {
			ret2 = balance(a2, b2);

			//둘이 같음 = ww -> a[2] = w or f
			if (ret2 == 2) {
				normal_coin.push_back(b[0]);
				a2[0] = b[2];
				int ret3 = balance(a2, b2);
				compare2(ret3, a2, b2, 1, error_coin, normal_coin, same_coin);
			}
			//b[0] = w, b[1] = f, b[2] = w
			else if (ret2 == 1) {
				normal_coin.push_back(b[2]);
				normal_coin.push_back(b[0]);
				error_coin.push_back(b[1]);
			}
			//b[0] = f, b[1] = w, b[2] = w
			else if (ret2 == 0) {
				normal_coin.push_back(b[2]);
				normal_coin.push_back(b[1]);
				error_coin.push_back(b[0]);
			}

			break;
		}
			  //뒤에가 가벼운 = fff
		case 1:
			error_coin.push_back(b[0]);
			error_coin.push_back(b[1]);
			error_coin.push_back(b[2]);

			break;

			//얘도 wff
		case 2:
			ret2 = balance(a2, b2);
			//b[0] = b[1] =f b[2] = w
			if (ret2 == 2) {
				error_coin.push_back(b[0]);
				error_coin.push_back(b[1]);
				normal_coin.push_back(b[2]);
			}
			else if (ret2 == 1) {
				error_coin.push_back(b[1]);
				normal_coin.push_back(b[0]);
				error_coin.push_back(b[2]);
			}
			else if (ret2 == 0) {
				error_coin.push_back(b[0]);
				normal_coin.push_back(b[1]);
				error_coin.push_back(b[2]);
			}
			else {
				cout << "errrrorr in compare6-2";
			}
			break;
		default:
			cout << "errrrorr in compare6-1";
			break;
		}
	}

}
void compare12(int ret, int a[7], int b[7], int ver, vector<int>& error_coin, vector<int>& normal_coin, vector<int>& same_coin)
{
	//wwwwwf
	if (ver == 1) {
		switch (ret)
		{
			//뒤가 무거움-> wwwwww
		case 0:
			for (int i = 0; i < 6; i++) {
				normal_coin.push_back(b[i]);
			}
			break;
			//앞에가 무거움 -> f가 하나 이상 있다는 것만 알음
		case 1: {
			int wwf[4] = { -1,-1,-1,-1 };
			wwf[0] = normal_coin[0];
			wwf[1] = normal_coin[1];
			wwf[2] = error_coin[0];

			int a3[4]; int b3[4];
			a3[0] = b[0]; a3[1] = b[1]; a3[2] = b[2]; a3[3] = -1;
			b3[0] = b[3]; b3[1] = b[4]; b3[2] = b[5]; b3[3] = -1;

			int ret2 = balance(wwf, a3);
			compare6(ret2, wwf, a3, 1, error_coin, normal_coin, same_coin);

			//a3 = www , b3은 wff or fff
			if (ret2 == 0) {
				int a2[2] = { 0,-1 };
				int b2[2] = { 0,-1 };
				a2[0] = b3[0];
				b2[0] = b3[1];
				int ret3 = balance(a2, b2);
				//둘이 같음 = FF -> a[2] = w or f
				if (ret3 == 2) {
					error_coin.push_back(b3[0]);
					a2[0] = b3[2];
					int ret4 = balance(a2, b2);
					compare2(ret4, a2, b2, 0, error_coin, normal_coin, same_coin);
				}
				//b3[0] = w, b3[1] = f, b3[2] = f
				else if (ret3 == 1) {
					error_coin.push_back(b3[2]);
					normal_coin.push_back(b3[0]);
					error_coin.push_back(b3[1]);
				}
				//b3[0] = f, b3[1] = w, b3[2] = f
				else if (ret3 == 0) {
					error_coin.push_back(b3[2]);
					normal_coin.push_back(b3[1]);
					error_coin.push_back(b3[0]);
				}
			}

			//a3 = wff or fff -> b3은 아무거나
			else if (ret2 == 1) {
				int ret3 = balance(wwf, b3);
				compare6(ret3, wwf, b3, 1, error_coin, normal_coin, same_coin);
			}

			//a3 = wwf -> b3은 wwf, wff, fff
			else if (ret2 == 2) {
				wwf[1] = error_coin[1]; //wwf -> wff로 바꿈
				int ret3 = balance(wwf, b3);

				if (ret3 == 2) {	//wff
					int a2[2] = { 0,-1 };
					int b2[2] = { 0,-1 };
					a2[0] = b3[0];
					b2[0] = b3[1];
					int ret4 = balance(a2, b2);
					//b3[0] = b3[1] =f b3[2] = w
					if (ret4 == 2) {
						error_coin.push_back(b3[0]);
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[2]);
					}
					else if (ret4 == 1) {
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[0]);
						error_coin.push_back(b3[2]);
					}
					else if (ret4 == 0) {
						error_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						error_coin.push_back(b3[2]);
					}
					else {
						cout << "errrrorr in compare12-2-2";
					}
				}
				else if (ret3 == 1) {	//fff
					error_coin.push_back(b3[0]);
					error_coin.push_back(b3[1]);
					error_coin.push_back(b3[2]);
				}
				else if (ret3 == 0) { //wwf
					int a2[2] = { 0,-1 };
					int b2[2] = { 0,-1 };
					a2[0] = b3[0];
					b2[0] = b3[1];
					int ret4 = balance(a2, b2);
					//b3[0] = b3[1] =w b3[2] = f
					if (ret4 == 2) {
						normal_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						error_coin.push_back(b3[2]);
					}
					else if (ret4 == 1) {
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[0]);
						normal_coin.push_back(b3[2]);
					}
					else if (ret4 == 0) {
						error_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						normal_coin.push_back(b3[2]);
					}
					else {
						cout << "errrrorr in compare12-2-0";
					}
				}
			}
		}
			  break;
			  //f 1개 껴있음-> a,b,c로 나누어서
		case 2: {
			int a2[3]; int b2[3]; int c2[3];

			a2[0] = b[0]; a2[1] = b[1]; a2[2] = -1;
			b2[0] = b[2]; b2[1] = b[3]; b2[2] = -1;
			c2[0] = b[4]; c2[1] = b[5]; c2[2] = -1;

			int ret2 = balance(a2, b2);
			//둘다 ww ww였던것
			if (ret2 == 2) {

				for (int i = 0; i < 2; i++) {
					normal_coin.push_back(a2[i]);
					normal_coin.push_back(b2[i]);
				}	//c비교
				int a1[2]; int b1[2];
				a1[0] = c2[0]; b1[0] = c2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 1, error_coin, normal_coin, same_coin);
			}
			else if (ret2 == 1) {
				for (int i = 0; i < 2; i++) {
					normal_coin.push_back(a2[i]);
					normal_coin.push_back(c2[i]);
				}
				//b비교
				int a1[2]; int b1[2];
				a1[0] = b2[0]; b1[0] = b2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 1, error_coin, normal_coin, same_coin);
			}
			else if (ret2 == 0) {
				for (int i = 0; i < 2; i++) {
					normal_coin.push_back(b2[i]);
					normal_coin.push_back(c2[i]);
				}
				//a비교
				int a1[2]; int b1[2];
				a1[0] = a2[0]; b1[0] = a2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 1, error_coin, normal_coin, same_coin);
			}
		}
			  break;
		default:
			printf("error occurs");
			break;

		}
	}
	else if (ver == 2) {
		switch (ret)
		{
			//뒤가 무거움 -> w가 하나 이상 있다는 것만 알음.
		case 0: {
			int wff[4] = { -1,-1,-1,-1 };
			wff[0] = normal_coin[0];
			wff[1] = error_coin[1];
			wff[2] = error_coin[0];

			int a3[4]; int b3[4];
			a3[0] = b[0]; a3[1] = b[1]; a3[2] = b[2]; a3[3] = -1;
			b3[0] = b[3]; b3[1] = b[4]; b3[2] = b[5]; b3[3] = -1;
			int ret2 = balance(wff, a3);
			compare6(ret2, wff, a3, 2, error_coin, normal_coin, same_coin);

			//a3 = wwf, www , b3은 아무거나
			if (ret2 == 0) {
				int ret3 = balance(wff, b3);
				compare6(ret3, wff, b3, 2, error_coin, normal_coin, same_coin);
			}
			//a3 = fff, b3 -> wwf, www
			else if (ret2 == 1) {
				int a2[2] = { 0,-1 };
				int b2[2] = { 0,-1 };
				a2[0] = b3[0];
				b2[0] = b3[1];
				int ret3 = balance(a2, b2);
				//둘이 같음 = ww -> a[2] = w or f
				if (ret3 == 2) {
					normal_coin.push_back(b3[0]);
					a2[0] = b3[2];
					int ret4 = balance(a2, b2);
					compare2(ret4, a2, b2, 1, error_coin, normal_coin, same_coin);
				}
				//b3[0] = w, b3[1] = f, b3[2] = w
				else if (ret3 == 1) {
					error_coin.push_back(b3[1]);
					normal_coin.push_back(b3[0]);
					normal_coin.push_back(b3[2]);
				}
				//b3[0] = f, b3[1] = w, b3[2] = w
				else if (ret3 == 0) {
					error_coin.push_back(b3[0]);
					normal_coin.push_back(b3[1]);
					normal_coin.push_back(b3[2]);
				}
			}
			//a3 = wff,-> b3 www wwf wff
			else if (ret2 == 2) {
				wff[1] = normal_coin[1]; //wff wwf로 바꿈
				int ret3 = balance(wff, b3);

				if (ret3 == 2) {	//wwf
					int a2[2] = { 0,-1 };
					int b2[2] = { 0,-1 };
					a2[0] = b3[0];
					b2[0] = b3[1];
					int ret4 = balance(a2, b2);
					//b3[0] = b3[1] =w b3[2] = f
					if (ret4 == 2) {
						normal_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						error_coin.push_back(b3[2]);
					}
					else if (ret4 == 1) {
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[0]);
						normal_coin.push_back(b3[2]);
					}
					else if (ret4 == 0) {
						error_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						normal_coin.push_back(b3[2]);
					}
					else {
						cout << "errrrorr in compare12-2-2";
					}
				}
				else if (ret3 == 1) {	//wff
					int a2[2] = { 0,-1 };
					int b2[2] = { 0,-1 };
					a2[0] = b3[0];
					b2[0] = b3[1];
					int ret4 = balance(a2, b2);
					//b3[0] = b3[1] =f b3[2] = w
					if (ret4 == 2) {
						error_coin.push_back(b3[0]);
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[2]);
					}
					else if (ret4 == 1) {
						error_coin.push_back(b3[1]);
						normal_coin.push_back(b3[0]);
						error_coin.push_back(b3[2]);
					}
					else if (ret4 == 0) {
						error_coin.push_back(b3[0]);
						normal_coin.push_back(b3[1]);
						error_coin.push_back(b3[2]);
					}
					else {
						cout << "errrrorr in compare12-2-0";
					}
				}
				else if (ret3 == 0) { //www
					normal_coin.push_back(b3[0]);
					normal_coin.push_back(b3[1]);
					normal_coin.push_back(b3[2]);
				}

			}
		}
			  break;
			  //앞이 무거움 -> ffffff
		case 1:
			for (int i = 0; i < 6; i++) {
				error_coin.push_back(b[i]);
			}
			break;
			//w 1개 껴있음-> a,b,c로 나누어서
		case 2: {
			int a2[3]; int b2[3]; int c2[3];

			a2[0] = b[0]; a2[1] = b[1]; a2[2] = -1;
			b2[0] = b[2]; b2[1] = b[3]; b2[2] = -1;
			c2[0] = b[4]; c2[1] = b[5]; c2[2] = -1;
			int ret2 = balance(a2, b2);
			//둘다 ff ff였던것
			if (ret2 == 2) {
				for (int i = 0; i < 2; i++) {
					error_coin.push_back(a2[i]);
					error_coin.push_back(b2[i]);
				}	//c비교
				int a1[2]; int b1[2];
				a1[0] = c2[0]; b1[0] = c2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 0, error_coin, normal_coin, same_coin);
			}
			else if (ret2 == 0) {
				for (int i = 0; i < 2; i++) {
					error_coin.push_back(a2[i]);
					error_coin.push_back(c2[i]);
				}
				//b비교
				int a1[2]; int b1[2];
				a1[0] = b2[0]; b1[0] = b2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 0, error_coin, normal_coin, same_coin);
			}
			else if (ret2 == 1) {
				for (int i = 0; i < 2; i++) {
					error_coin.push_back(b2[i]);
					error_coin.push_back(c2[i]);
				}
				//a비교
				int a1[2]; int b1[2];
				a1[0] = a2[0]; b1[0] = a2[1]; a1[1] = -1; b1[1] = -1;
				int ret3 = balance(a1, b1);
				compare2(ret3, a1, b1, 0, error_coin, normal_coin, same_coin);
			}
		}
			  break;
		default:
			printf("error occurs");
			break;
		}
	}
}
//10P ,90P

int* at50() {

	vector<int> error_coin;
	vector<int> normal_coin;
	//100,2짜리 선언
	vector<vector<int>> same_coin;

	int a[2] = { 0,-1 };
	int b[2] = { 1,-1 };
	int i = 0;
	int ret;

	while (i < 100) {
		a[0] = i++;
		b[0] = i++;
		ret = balance(a, b);

		switch (ret)
		{
		case 0: //a에 불량동전
			error_coin.push_back(a[0]);
			normal_coin.push_back(b[0]);
			break;
		case 1://b에 불량동전
			error_coin.push_back(b[0]);
			normal_coin.push_back(a[0]);
			break;
		case 2: {
			vector<int> push;
			push.push_back(a[0]);
			push.push_back(b[0]);
			same_coin.push_back(push);
		}
			  break;
		default:
			printf("error occurs");
			break;
		}
	}
	SeparateSame(same_coin, 2, same_coin.size(), normal_coin, error_coin);

	int size = error_coin.size();
	int* ans = (int*)malloc(sizeof(int) * (size + 1));;
	for (int i = 0; i < size; i++) {
		ans[i] = error_coin[i];
	}
	ans[size] = -1;

	/*sort(ans, ans + size);
	for (int i = 0; i < size; i++) {
		cout << ans[i] << " ";
	}*/


	return ans;
}
void SeparateSame(vector<vector<int>> samecoin, int round, int size, vector<int>& normal_coin, vector<int>& error_coin) {

	if (size == 0) return;

	int* a = (int*)malloc(sizeof(int) * round + 1);
	int* b = (int*)malloc(sizeof(int) * round + 1);
	vector<vector<int>> samecoin2;

	int ret = 0;
	a[round] = -1;
	b[round] = -1;

	for (int x = 0; x <= size - 2; x = x + 2) {

		for (int i = 0; i < round; i++) {
			a[i] = samecoin[x][i];
			b[i] = samecoin[x + 1][i];
		}


		ret = balance(a, b);

		switch (ret)
		{
		case 0: //a에 불량동전
			for (int k = 0; k < round; k++) {
				error_coin.push_back(a[k]);
			}
			for (int k = 0; k < round; k++) {
				normal_coin.push_back(b[k]);
			}
			break;

		case 1://b에 불량동전
			for (int k = 0; k < round; k++) {
				error_coin.push_back(b[k]);
			}
			for (int k = 0; k < round; k++) {
				normal_coin.push_back(a[k]);
			}
			break;

			//둘다 같음
		case 2: {
			vector<int> forsamecoin;
			for (int k = 0; k < round; k++) {
				forsamecoin.push_back(a[k]);
			}
			for (int k = 0; k < round; k++) {
				forsamecoin.push_back(b[k]);
			}
			samecoin2.push_back(forsamecoin);
			break;
		}


		default:
			printf("error occurs");
			break;
		}
	}

	//size가 홀수 -> 마지막에 따로 비교
	if ((size % 2) != 0) {
		int normal[2]; normal[0] = normal_coin.front(); normal[1] = -1;
		int findsame[2]; findsame[1] = -1;

		for (int i = 0; i < round; i++) {
			a[i] = samecoin[size - 1][i];
		}
		findsame[0] = a[0];

		ret = balance(findsame, normal);

		switch (ret)
		{
		case 0: //a에 불량동전
			for (int k = 0; k < round; k++) {
				error_coin.push_back(a[k]);
			}
			break;

		case 1://에러
			printf("error occurs in %d", round);
			break;

		case 2: //둘다 같음
			for (int k = 0; k < round; k++) {
				normal_coin.push_back(a[k]);
			}
			break;
		default:
			printf("error occurs");
			break;
		}
	}

	if (!samecoin2.empty()) {
		//재귀로 호출 (same ww ww ff ff -> wwww ffff -> wwwwwwww ffffffff ....)
		SeparateSame(samecoin2, round * 2, samecoin2.size(), normal_coin, error_coin);
		return;
	}
	return;
}
//50P