# 불량동전 찾기 

서울시립대학교 3-1 '알고리즘' 팀프로젝트

팀명 : 알고맀즘



## 수정 사항

2020-05-20 우희은

- lowerP.cpp, lowerP.header 추가
- coin.cpp에서 결과 반환 시 arr 받는 부분을 통일하기 위해 A, B 변수 사용


2020-05-20 장영선
- 기존 coin.cpp 파일을 bad_coin.cpp과 bad_coin.h로 분리
- main.cpp 새로 생성
- 각자 코드 작성할 때 별도의 파일에 정답이 담긴 배열의 포인터를 리턴하는 함수를 작성한 뒤 main.cpp에 추가해주세요.
`int *function();`


2020-05-21 우희은

- windows 환경에서는 'bad_coin.cpp'의 Windows.h 파일을 include 하는 구문을 주석처리해주세요.
- 또한 버전 오류가 생긴다면 솔루션 탐색기에서 솔루션 오른쪽 마우스버튼 => 솔루션 대상 변경 => 16 or 17 중 현재 버전이 아닌 것으로 변경
- 솔루션 탐색기에 파일이 몇 개 없다면 직접 파일들을 추가해서 실행시키면 됩니다.


2020-05-21 장영선
- count.py 추가
- p에 따라 발생되는 `불량 동전의 수`에 따라서 실행횟수를 분류하였습니다.
- count.py main 함수 제일 상단의 실행파일과 out파일 path를 변경해주세요.
- p_start, p_end와 rep를 설정해주세요.
- 저장한 뒤 실행해주시면 됩니다.
- 파일로 저장, 그래프 그리는 것은 추후 추가 예정


2020-05-21 엄현식
- 50P.cpp ,50P.h, 10P.h, 10P.cpp 추가 
- main함수 수정
- 50P는 47~50 / 10P는 5~10 / 90~95에서 작동하게 했습니다
- 프로젝트 한번 실행하고 올려서 솔루션 파일 변경되었습니다.


2020-05-21 장영선
- count.py 수정(plotting, 실행중 내용과 실행 결과 파일에 저장)
- count.py의 main 함수 최상단의 설정들만 변경한 뒤 실행해주시면 됩니다.
- 실행파일 실행 시 매개변수 사용해서 p값 받아들이도록 수정(main.cpp)


2020-05-21 우희은
- 모든 VS 관련 file 삭제, Project 관련 파일만 남겨둠

2020-05-22 장영선

- count.py 수정(에러 로그 저장, tmp 파일 삭제하지 않음)
- 에러는 err_file_path에 설정한 파일에서 확인. 로그파일은 에러 파일 내에 기재되어 있음

2020-05-23 엄현식

* main에서 P에 따른 확률 세부조정 (lowerP ,10P, 90P, 50P)
* 알고리즘 분석 폴더 추가 (간단한 알고리즘을 분석한 md파일 게시)

2020-05-23 우희은

* lowerP.cpp 코드 수정
* 1<=p<=4, 96<=p<=99 에서 동작하게 수정

2020-05-26 우희은
* lowerP 코드를 main.cpp 로 합침
* lowerP.cpp, lowerP.h 삭제

2020-05-26 엄현식

* 10P , 50P 다 합친 파일 최종본 main.c 완성


## CoinProjectAlgo 사용법
파일들을 다운로드 받은 후, VS IDE에서 오른쪽 솔루션탐색기에 모든 파일 추가
=> 솔루션 탐색기에서 마우스 오른쪽 버튼 클릭 후 기존 파일 추가로 추가할 수 있습니다.

- obj 파일 => 리소스 파일에 추가
- cpp 파일 => 소스 파일에 추가
- h 파일 => 헤더 파일에 추가

## count.py 사용법
- exe_file_path, out_file_path_tmp, mid_file_path, res_file_path, err_file_path, plot_file_path 설정
- p_start, p_end, rep 설정
<br>ex) p_start = 21, p_end = 41, rep=20 이면 p=21~40까지 각 20번씩 반복
- 프로그램 실행이 종료되면 err_file_path를 확인하여 에러 발생 확인
- windows에서 파일 경로 예시 : out_file_path_tmp = r"abcde\tmp{0}.txt" (앞에 r붙여야 \ 이거 인식합니다)
- 파일 프로세스 에러 시 #os.remove(out_file_path) -> 주석처리 하거나, 파일 닫고 remove시키면 됩니다!