import os
import sys
import re

from matplotlib import pyplot as plt # matplotlib 설치 필요


def main():
    # 파일 경로 설정
    exe_file_path = "/algorithm/team/main" # 실행 파일
    out_file_path_tmp = "/algorithm/python/tmp{0}.txt" # output 파일. {0}은 지우시면 안됩니다.
    mid_file_path = "/algorithm/python/mid.txt" # 실행 중 발생되는 값들 저장용
    res_file_path = "/algorithm/python/res.txt" # 결과 파일
    err_file_path = "/algorithm/python/err.txt" # err 발생한 곳 뽑아주는 파일
    plot_file_path = "/algorithm/python/plot.png" # 그래프

    # 원하는 p 범위 설정: [p_start, p_end). p_start는 1부터, p_end는 100까지
    p_start = 96
    p_end = 100

    # 반복 횟수 설정
    rep = 30



    #--- 불량 동전의 개수 1부터 99까지 딕셔너리 생성
    #--- 에러 발생 횟수 카운트할 딕셔너리 생성
    res = {}
    err = {}
    for i in range(1, 100):
        res[i] = []
        err[i] = 0

    #--- p=[p_start,p_end) rep번씩 돌린 뒤 각각 평균, 전체 평균 내기
    # 실행 중 발생되는 값들을 저장하기 위한 파일 오픈
    coin_num = None
    mid_file = open(mid_file_path, "w+")
    # 에러 저장 파일 오픈
    err_file = open(err_file_path, "w+")
    for i in range(p_start, p_end):
        for j in range(1, rep+1):
            step = "p={0}, {1}/{2}번째".format(str(i), str(j), str(rep))
            print(step)
            mid_file.write(step + "\n")
            
            # 실행파일 실행한 뒤 파일에 임시 저장
            number = i * 10000 + j
            out_file_path = out_file_path_tmp.format(str(number))
            os.system(exe_file_path + ' {0} > {1}'.format(str(i), out_file_path))
            
            # 임시 저장된 실행 결과 불러오기
            out_file = open(out_file_path, "r")
            lines = out_file.readlines()

            #--- 불량 동전 개수 파싱
            p = re.compile('(Number of bad coins : )([0-9]+)')
            for line in lines:
                coin_num = p.search(line)
                if coin_num != None:
                    coin_num = coin_num.group(2)
                    break
            out = "불량동전: " + coin_num
            mid_file.write(out + "\n")
            coin_num = int(coin_num)

            #--- 결과 파싱
            p = re.compile("([0-9]+)(회 님은 성공하셨습니다.)")
            weighing_num = p.search(lines[-1])
            # 결과 에러
            if weighing_num == None:
                res[coin_num].append(lines[-1])
                out = "에러발생: " + lines[-1]
                print(out)
                mid_file.write(out + "\n\n")
                err[coin_num] += 1
                # 에러 기록
                err_file.write(step + "\n")
                err_file.write(out)
                err_file.write("로그: " + out_file_path + "\n\n")
            # 결과 정상
            else:
                weighing_num = weighing_num.group(1)
                out = "저울횟수: " + weighing_num
                mid_file.write(out + "\n\n")
                weighing_num = int(weighing_num)
                res[coin_num].append(weighing_num)
                # 정상이면 임시 파일 삭제함. 정상일 때의 로그도 보고 싶으면 아래 1줄 주석처리
                os.remove(out_file_path)


            out_file.close()

    # 실행 중 발생 값 저장 파일 닫기
    mid_file.close()
    # 에러 저장 파일 닫기
    err_file.close()
    # 결과 저장용 파일 오픈
    res_file = open(res_file_path, "w+")

    
    #--- 결과 출력
    avg = {}
    for i in range(1, 100):
        if len(res[i]) != 0: # 한 번 이상 나온 경우만 출력
            avg[i] = 0
            normal = 0
            # 에러가 아닐 때 파란색 점으로 실행 결과 찍기
            for r in res[i]:
                if type(r) != type('str'):
                    plt.scatter(i, r, marker='.', c='b')
                    normal += 1
                    avg[i] += r
            # 에러만 있었으면 평균 딕셔너리 삭제
            if normal == 0:
                del(avg[i])
                out = "불량동전 %02d개 - 총 %02d번, 에러 %02d번" % (i, len(res[i]), err[i])
            # 에러 외에 결과 있었음
            else:
                avg[i] /= normal
                out = "불량동전 %02d개 - 총 %02d번, 에러 %02d번, 에러 제외 평균 %02d회" % (i, len(res[i]), err[i], avg[i])
            print(out)
            res_file.write(out + "\n")

    # 결과 저장용 파일 닫기
    res_file.close()
    

    #--- 그래프
    # 빨간색 별로 평균 찍기
    avg_x, avg_y = zip(*avg.items())
    plt.scatter(avg_x, avg_y, marker='*', c='r', label='avg')
    # 로워바운드 그려넣기
    lower_x = range(1, 100)
    lower_y = [5, 8, 11, 14, 17, 20, 22, 24, 26, 28,
               30, 32, 34, 35, 37, 38, 40, 41, 43, 44, 
               45, 46, 47, 49, 50, 50, 51, 52, 53, 54,
               55, 55, 56, 57, 57, 58, 58, 59, 59, 59, 
               60, 60, 60, 61, 61, 61, 61, 61, 61, 61,
               61, 61, 61, 61, 61, 61, 60, 60, 60, 59,
               59, 59, 58, 58, 57, 57, 56, 55, 55, 54,
               53, 52, 51, 50, 50, 49, 47, 46, 45, 44,
               43, 41, 40, 38, 37, 35, 34, 32, 30, 28, 
               26, 24, 22, 20, 17, 14, 11, 8, 5]
    plt.plot(lower_x, lower_y, color='black', label='lower bound')
    # 그래프 이름 설정
    plt.title("Counterfeit Coins")
    # 그래프 범위 설정
    plt.xlim(0, 101)
    plt.ylim(0, 100)
    # 그래프 축 쪼개기
    plt.xticks([10, 20, 30, 40, 50, 60, 70, 80, 90])
    plt.yticks([10, 20, 30, 40, 50, 60, 70, 80, 90])
    # 그래프 축 이름
    plt.xlabel('# of Counterfeit coins')
    plt.ylabel('# of weighing')
    # 그래프 범례
    plt.legend(loc='lower center')
    # 격자 사용
    plt.grid(True)
    # 그래프 저장
    plt.savefig(plot_file_path)



if __name__ == "__main__":
    main()