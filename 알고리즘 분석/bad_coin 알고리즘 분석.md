## bad_coin 알고리즘 분석

##### array normal_coin

> 정상동전으로 분류된 동전 저장

##### array bad_coin

> 불량동전으로 분류된 동전 저장

##### array same_pair

> two_classifi()에서 무게가 같다고 판단된 동전 쌍 저장



##### badcoin_less_than_50()

> 10<p<47 에서 실행되는 메인함수
>
> 1. 동전을 2개씩 묶은 후 무게가 다르다고 나올때 까지 비교(**two classifi()**)
>
>    무게가 같다고 나온 것들은 same_pair에 임시저장해둠.
>
> 2. same_pair에 있는 동전쌍을 정상동전or 불량동전 으로 분류 (**same_pair_classifi()**)
>
> 3. 위 과정에서 정상동전이 2개 이상이 나왔다면 **three_normal_classifi()** 실행
>
>    미만이라면 정상동전이 2개가 될때까지 **two_item_classifi()** 실행 후 실행
>
> 4. 3쌍으로 묶이지 못한 남은 동전을 세고 종료

##### badcoin_more_than_50()

> 53<p<90 에서실행되는 메인함수
>
>1. 동전을 2개씩 묶은 후 무게가 다르다고 나올때 까지 비교(**two classifi()**)
>
>   무게가 같다고 나온 것들은 same_pair에 임시저장해둠.
>
>2. same_pair에 있는 동전쌍을 정상동전or 불량동전 으로 분류  (**same_pair_classifi()**)
>
>3. 위 과정에서 불량동전이 2개 이상이 나왔다면 **three_bad_classifi()** 실행
>
>   미만이라면 불량동전이 2개가 될때까지 **two_item_classifi()** 실행 후 실행
>
>4. 비교하지 못한 남은 동전을 세고 종료

##### 

##### two_classifi()

> 불량동전과 정상동전이 하나도 밝혀지지 않았을때 사용됨.
>
> 1. 동전을 2개씩 묶은 후 비교
> 2. 무게가 같다고 나오면 same_pair array에 넣고 다르다고 나오면 가벼운 쪽의 동전 두개를 비교해 정상,불량 동전을 찾아냄

##### same_pair_classifi()

> two_classifi()에서 same_pair로 분류 된 동전쌍들을 정상-불량으로 분류함
>
> 이미 밝혀진 (정상, 불량)쌍과 비교함. 
>
> 1~3회의 비교로 4개의 동전이 분류됨. 

##### two_item_classifi()

> three_normal_classifi()나 three_bad_classifi() 실행 전 불량or 정상동전의 개수가 충분하지 않을때 사용.
>
> 이미 밝혀진 (정상,불량)쌍과 비교함. 
>
> 1~2회의 비교로 2개의 동전이 분류됨.

##### three_normal_classifi()

> 이미 밝혀진 (정상,정상,불량)쌍과 비교하여 분류함
>
> 1~3회의 비교로 3개의 동전이 분류됨.
>
> N개의 동전이 모두 (정상,정상,정상)이라면 총 N/3회로 모든 동전이 분류됨.
>
> N개의 동전이 모두 (불량, 불량, 불량) or (불량,불량,정상)이라면 총 N회로 모든 동전이 분류됨.

##### three_bad_classfi()

> 이미 밝혀진 (정상,불량,불량)쌍과 비교하여 분류함
>
> 1~3회의 비교로 3개의 동전이 분류됨
>
> N개의 동전이 모두 (불량,불량,불량)이라면 총 N/3회로 모든 동전이 분류됨.
>
> N개의 동전이 모두 (정상,정상,정상)or(정상,정상,불량)이라면 총 N회로 모든 동전이 분류됨.



## 복잡도 예측

최악의 경우

1. two_classifi()에서 마지막까지 same_pair가 나오는 경우, 각 pair가 모두(정상,불량)쌍 인경우 4번의 비교로 4개의 동전이 분류됨.
2. three_normal_classifi()에서 모든 동전쌍이 (불량,불량,불량)or (불량,불량,정상)일 경우 3번의 비교로 3개의 동전이 분류됨
3. three_bad_classifi()에서 모든 동전쌍이 (정상,정상,정상)or(정상,정상,불량)일 경우 3번의 비겨로 3개의 동전이 분류됨

==>이 세 경우에서는 N개의 동전분류를 위해 N번의 비교가 필요함.O(N)



최선의 경우

1. three_normal_classifi()에서 모든 동전쌍이 (정상,정상,정상)일 경우 1번의 비교로 3개의 동전이 분류됨
2. three_bad_classifi()에서 모든 동전쌍이 (불량,불량,불량)일 경우 3번의 비겨로 3개의 동전이 분류됨

==>이 두 경우에서는 N개의 동전분류를 위해 N/3번의 비교가 필요함.O(N/3)



## 실행결과

three_normal_classifi()의 경우 불량동전의 개수가 많을수록 성능이 떨어지고

three_bad_classifi()의 경우 정상동전의 개수가 많을수록 성능이 떨어짐.

각각 가장 성능이 떨어지는 p=46, p=54에서 실행시키면



p=46 : 72, 67,63,72,67,72,74,68,71,72 => 69.8회

p=54 : 73,68,71,69,69,64,65,69,66,71=> 68.5 회  의 결과가 나타남.